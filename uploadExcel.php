<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$timestamp = time();

include 'selectFilecss.php';
include 'js.php';

require_once('vendor/php-excel-reader/excel_reader2.php');
require_once('vendor/SpreadsheetReader.php');

if (isset($_POST["import"]))
{
  $allowedFileType = ['application/vnd.ms-excel','text/xls','text/xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];

  if(in_array($_FILES["file"]["type"],$allowedFileType))
  {
    $targetPath = 'uploadsExcel/'.$timestamp.$_FILES['file']['name'];
    move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);
    $Reader = new SpreadsheetReader($targetPath);
    $sheetCount = count($Reader->sheets());
    for($i=0;$i<$sheetCount;$i++)
    {
      $Reader->ChangeSheet($i);
      foreach ($Reader as $Row)
      {
        $month = "";
        if(isset($Row[0])) 
        {
          $month = mysqli_real_escape_string($conn,$Row[0]);
        }
        $year = "";
        if(isset($Row[0])) 
        {
          $year = mysqli_real_escape_string($conn,$Row[1]);
        }
        $fullname = "";
        if(isset($Row[0])) 
        {
          $fullname = mysqli_real_escape_string($conn,$Row[2]);
        }
        $accountNo = "";
        if(isset($Row[0])) 
        {
          $accountNo = mysqli_real_escape_string($conn,$Row[3]);
        }
        $status = "";
        if(isset($Row[0])) 
        {
          $status = mysqli_real_escape_string($conn,$Row[4]);
        }
        $designation = "";
        if(isset($Row[0])) 
        {
          $designation = mysqli_real_escape_string($conn,$Row[5]);
        }
        $epfNo = "";
        if(isset($Row[0])) 
        {
          $epfNo = mysqli_real_escape_string($conn,$Row[6]);
        }
        $basicPay = "";
        if(isset($Row[0])) 
        {
          $basicPay = mysqli_real_escape_string($conn,$Row[7]);
        }
        $unpaidLeave = "";
        if(isset($Row[0])) 
        {
          $unpaidLeave = mysqli_real_escape_string($conn,$Row[8]);
        }
        $salaryAfterUL = "";
        if(isset($Row[0])) 
        {
          $salaryAfterUL = mysqli_real_escape_string($conn,$Row[9]);
        }
        $epfComp = "";
        if(isset($Row[0])) 
        {
          $epfComp = mysqli_real_escape_string($conn,$Row[10]);
        }
        $epf = "";
        if(isset($Row[0])) 
        {
          $epf = mysqli_real_escape_string($conn,$Row[11]);
        }
        $perkesoComp = "";
        if(isset($Row[0])) 
        {
          $perkesoComp = mysqli_real_escape_string($conn,$Row[12]);
        }
        $perkeso = "";
        if(isset($Row[0])) 
        {
          $perkeso = mysqli_real_escape_string($conn,$Row[13]);
        }
        $eisComp = "";
        if(isset($Row[0])) 
        {
          $eisComp = mysqli_real_escape_string($conn,$Row[14]);
        }
        $eis = "";
        if(isset($Row[0])) 
        {
          $eis = mysqli_real_escape_string($conn,$Row[15]);
        }
        $pcb = "";
        if(isset($Row[0])) 
        {
          $pcb = mysqli_real_escape_string($conn,$Row[16]);
        }
        $allowance = "";
        if(isset($Row[0])) 
        {
          $allowance = mysqli_real_escape_string($conn,$Row[17]);
        }
        $commission = "";
        if(isset($Row[0])) 
        {
          $commission = mysqli_real_escape_string($conn,$Row[18]);
        }
        $bonus = "";
        if(isset($Row[0])) 
        {
          $bonus = mysqli_real_escape_string($conn,$Row[19]);
        }
        $overtimeCost = "";
        if(isset($Row[0])) 
        {
          $overtimeCost = mysqli_real_escape_string($conn,$Row[20]);
        }
        $netMonthPay = "";
        if(isset($Row[0])) 
        {
          $netMonthPay = mysqli_real_escape_string($conn,$Row[21]);
        }

        $uid = md5(uniqid());

        if (!empty($month) || !empty($year) || !empty($fullname) || !empty($accountNo) || !empty($status) || !empty($designation) || !empty($epfNo) || !empty($basicPay) || !empty($unpaidLeave) || !empty($salaryAfterUL) || !empty($epfComp) || !empty($epf) || !empty($perkesoComp) || !empty($perkeso) || !empty($eisComp) || !empty($eis) || !empty($pcb) || !empty($allowance) || !empty($commission) || !empty($bonus) || !empty($overtimeCost) || !empty($netMonthPay))
        {
          $query = "INSERT INTO payment (uid,month,year,fullname,account_no,status,designation,epf_no,basic_pay,unpaid_leave,salary_after_ul,epf_comp,epf,perkeso_comp,perkeso,eis_comp,eis,pcb,allowance,commission,bonus,overtime_cost,net_month_pay) 
                    VALUES ('".$uid."','".$month."','".$year."','".$fullname."','".$accountNo."','".$status."','".$designation."','".$epfNo."','".$basicPay."','".$unpaidLeave."','".$salaryAfterUL."','".$epfComp."','".$epf."','".$perkesoComp."','".$perkeso."','".$eisComp."','".$eis."','".$pcb."','".$allowance."','".$commission."','".$bonus."','".$overtimeCost."','".$netMonthPay."') ";
          $result = mysqli_query($conn, $query);
          if (! empty($result))
          {
            echo "<script>alert('Excel Uploaded !');window.location='../cms/adminStaffPS.php'</script>";        
          }
          else 
          {
            echo "<script>alert('Problem in Importing Excel Data !');window.location='../cms/uploadExcel.php'</script>";
          }
        }
      }
    }
  }
  else
  {
    echo "<script>alert('Invalid File Type. Upload Excel File.');window.location='../cms/uploadExcel.php'</script>";
  }
}
?>

<!DOCTYPE html>
<html>
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Payslip / Salary Details | CMS" />
    <title>Payslip / Salary Details | CMS | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'adminSidebar.php'; ?>
<div class="next-to-sidebar"> 

<h1 class="h1-title">Import Excel File</h1>

  <div class="outer-container">
    <form action="" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
      <label>Select File</label><br><input type="file" name="file" id="file" accept=".xls,.xlsx"><div class="clear"></div>
      <button type="submit" id="submit" name="import"  class="clean red-btn margin-top30 fix300-btn margin-left0">Submit</button>
      <div id="response" class="<?php if(!empty($type)) { echo $type . " display-block"; } ?>"><?php if(!empty($message)) { echo $message; } ?></div>
    </form>
  </div>

  <!-- <a href="template/payslip_format.xlsx" class="big-header-color" target="_blank" download>Download Payslip Excel Template</a> -->

<!-- <style>
.import-li{
color:#bf1b37;
background-color:white;}
.import-li .hover1a{
display:none;}
.import-li .hover1b{
display:block;}
</style> -->

<?php include 'js.php'; ?>
<script  src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
    </script>
</body>
</html>