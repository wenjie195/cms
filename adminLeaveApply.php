<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Name.php';
// require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allName = getName($conn);
// $allCategory = getCategory($conn);
$allUser = getUser($conn);
// $allUser = getUser($conn, "WHERE user_type = 1 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Apply Leave | CMS" />
    <title>Apply Leave | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">
	<h1 class="h1-title">Apply Leave</h1> 

    <!-- <form action="utilities/adminApplyLeaveFunction.php" method="POST"> -->
    <!-- <form action="utilities/adminApplyLeaveFunction.php" method="POST" enctype="multipart/form-data"> -->
    <form action="utilities/adminLeaveApplyFunction.php" method="POST" enctype="multipart/form-data">

        <div class="input50-div">
            <p class="input-title-p">Fullname</p>
            <!-- <input class="clean tele-input" type="text" placeholder="Fullname" id="fullname" name="fullname" required>       -->
            <select class="clean tele-input" value="<?php echo $allUser[0]->getFullname();?>" name="fullname" id="fullname" required>
                <option value="">Please Select a Name</option>
                <?php
                for ($cntAA=0; $cntAA <count($allUser) ; $cntAA++)
                {
                ?>
                    <option value="<?php echo $allUser[$cntAA]->getFullname(); ?>"> 
                        <?php echo $allUser[$cntAA]->getFullname(); ?>
                    </option>
                <?php
                }
                ?>
            </select>   
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Total Days</p>
            <input type="text" placeholder="Total Days" class="clean tele-input" name='total_days' id="total_days" required>
        </div> 

        <div class="clear"></div>

        <div class="input50-div">
            <p class="input-title-p">Start Date</p>
            <input type="date" placeholder="Start Date" class="clean tele-input" name='fromDate' id="fromDate" required>
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">End Date</p>
            <input type="date" placeholder="Start Date" class="clean tele-input" name='endDate' id="endDate" required>
        </div> 

        <div class="clear"></div>

        <div class="input50-div">
            <p class="input-title-p">Reason</p>
            <textarea  type="text" class="clean tele-input" placeholder="Reason" id="reason" name="reason"></textarea> 
        </div> 

        <div class="clear"></div>

        <div class="input50-div">
            <p class="input-title-p">Upload Document</p>     
            <!-- <p class="input-title-p">Upload Document 1</p>     -->
            <p><input id="file-upload" type="file" name="image_one" id="image_one" accept="image/*" /></p> 
        </div> 

        <!-- <div class="input50-div second-input50">
            <p class="input-title-p">Upload Document 1</p>     
            <p><input id="file-upload" type="file" name="image_two" id="image_two" accept="image/*" /></p> 
        </div>  -->

        <div class="clear"></div>

        <button class="clean red-btn margin-top30 fix300-btn margin-bottom" name="submit">Submit</button>

        <div class="clear"></div>
    </form>
</div>

<!-- <style>
.leave-li{
	color:#264a9c;
	background-color:white;}
.leave-li .hover1a{
	display:none;}
.leave-li .hover1b{
	display:block;}
</style> -->

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Leave Application Submitted <br> Pending For Approval"; 
            // $messageType = "<H2>GFYS</H2> <br><br> Your Leave is NOT APPROVE !!! <br><br> HAHAHAHAHA ..!.."; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Insufficent Leave !!"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Fail To Apply Leave !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !!","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
  $(function()
  {
    $("#fromDate").datepicker(
    {
    dateFormat:'yy-mm-dd',
    changeMonth: true,
    changeYear:true,
    }

    );
  });
</script>

<script>
  $(function()
  {
    $("#endDate").datepicker(
    {
      dateFormat:'yy-mm-dd',
      changeMonth: true,
      changeYear:true,
    }
    );
  });
</script>

</body>
</html>