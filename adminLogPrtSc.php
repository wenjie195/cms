<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/PageView.php';
require_once dirname(__FILE__) . '/classes/Printscreen.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$pageView = getPrintscreen($conn, "ORDER BY date_created DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Admin Log (Prt Sc) | CMS" />
    <title>Admin Log (Prt Sc) | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">

    <h1 class="h1-titlez"><a href="adminLogLogin.php" class="h1-title red-link">Admin Log (Login)</a> | <b>Admin Log (Prt Sc)</b></h1>

    <div class="clear"></div>

    <div class="big-four-input-container">
      <div class="width100">
        <p class="input-top-p">Staff Name</p>
        <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Staff Name" class="tele-four-input tele-input clean">
      </div>
    </div>

    <div class="clear"></div>

    <div class="width100 shipping-div2">
  
    <div class="overflow-scroll-div">
        <table class="shipping-table" id="myTable">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Staff Name</th>
                    <th>Page</th>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                if($pageView)
                {   
                    for($cnt = 0;$cnt < count($pageView) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>

                            <td>
                              <?php 
                                $userUid = $pageView[$cnt]->getUserUid();

                                $conn = connDB();
                                $userDetails = getUser($conn,"WHERE uid = ? ", array("uid") ,array($userUid),"s");
                                echo $username = $userDetails[0]->getUsername();
                              ?>
                            </td>

                            <td><?php echo $pageView[$cnt]->getPage();?></td>
                            <td><?php echo $pageView[$cnt]->getDateCreated();?></td>
                        </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
            </tbody>
        </table>
    </div>
    
    <div class="clear"></div>

</div>

<!-- <style>
.dashboard-li{
	color:#264a9c;
	background-color:white;}
.dashboard-li .hover1a{
	display:none;}
.dashboard-li .hover1b{
	display:block;}
</style> -->

<?php unset($_SESSION['quotation_session']); unset($_SESSION['invoice_session']); unset($_SESSION['url']);?>
<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputB");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputC");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>