<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/AddOnProduct.php';
require_once dirname(__FILE__) . '/classes/LeaveStatus.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $productDetails = getLeaveStatus($conn);
$productDetails = getLeaveStatus($conn, " ORDER BY date_created DESC ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="All Leave | CMS" />
    <title>All Leave | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">

    <!-- <h1 class="h1-title open">Quotation (Pending)</h1> -->
    <h1 class="h1-title open">Leave</h1>

    <a href='adminLeaveApply.php'><div class="blue-btn width175">Apply Leave</div></a>
    

    <div class="clear"></div>

    <div class="width100 shipping-div2 margin-top40">
  
    <div class="overflow-scroll-div">
        <table class="shipping-table" id="myTable">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Total Days</th>
                    <th>Reason</th>          
                    <th>Attach Document</th>                
                    <th>Status</th>
                    <th>Date</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                if($productDetails)
                {   
                    for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $productDetails[$cnt]->getName();?></td>

                            <td><?php echo $productDetails[$cnt]->getStartDate();?></td>
                            <td><?php echo $productDetails[$cnt]->getEndDate();?></td>
                            <td><?php echo $productDetails[$cnt]->getTotalDays();?></td>

                            <td><?php echo $productDetails[$cnt]->getReason();?></td>

                            <td>
                              <?php 
                                $docOne = $productDetails[$cnt]->getDocOne();
                                if($docOne != '')
                                {
                                ?>
                                  <a href="leaveDoc/<?php echo $productDetails[$cnt]->getDocOne();?>" title="Document"  class="dark-tur-link hover1 img-btn" target="_blank"><img src="img/view.png" style="width:20px;" class="hover1a" alt="View" title="View"  >
                                        <img src="img/view2.png" style="width:20px;" class="hover1b" alt="View" title="View" ></a>
                                <?php
                                }
                                else
                                {
                                  echo "No Document";
                                }
                              ?>
                            </td>

                            <td><?php echo $productDetails[$cnt]->getStatus();?></td>

                            <td>
                              <?php echo $date = date("Y-m-d",strtotime($productDetails[$cnt]->getDateCreated()));?>
                            </td>

                            <td>
                              <?php 
                                $status = $productDetails[$cnt]->getStatus();
                                if($status == 'Pending')
                                {
                                ?>
                                  <form action="utilities/adminLeaveApprovedFunction.php" method="POST"  style="display:inline-block; margin-right:5px;">
                                    <button class="clean hover1 img-btn" type="submit" name="leave_uid" value="<?php echo $productDetails[$cnt]->getUid();?>">
                                    <input type="hidden" value="<?php echo $productDetails[$cnt]->getUserUid();?>" name='user_uid' id="user_uid" readonly>
                                      <img src="img/tick0.png" class="width100 hover1a" alt="Approve" title="Approve" >
                                        <img src="img/tick2.png" class="width100 hover1b" alt="Approve" title="Approve">
                                    </button>
                                  </form>

                                  <form action="utilities/adminLeaveRejectFunction.php" method="POST" style="display:inline-block; ">
                                    <button class="clean hover1 img-btn" type="submit" name="leave_uid" value="<?php echo $productDetails[$cnt]->getUid();?>">
                                       <img src="img/close.png" class="width100 hover1a" alt="Reject" title="Reject" >
                                        <img src="img/close2.png" class="width100 hover1b" alt="Reject" title="Reject">
                                    </button>
                                  </form>
                                <?php
                                }
                                else
                                {}
                              ?>
                            </td>

                        </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
            </tbody>
        </table>
    </div>
    
    <div class="clear"></div>

</div>

<style>
.leave-li{
	color:#264a9c;
	background-color:white;}
.leave-li .hover1a{
	display:none;}
.leave-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Leave Application Submitted <br> Pending For Approval"; 
            // $messageType = "<H2>GFYS</H2> <br><br> Your Leave is NOT APPROVE !!! <br><br> HAHAHAHAHA ..!.."; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Insufficent Leave !!"; 
        }
        elseif($_GET['type'] == 3)
        {
            $messageType = "Fail To Apply Leave !!";
        }
        elseif($_GET['type'] == 4)
        {
            $messageType = "Leave Application Approved !!";
        }
        elseif($_GET['type'] == 5)
        {
            $messageType = "Leave Application Rejected !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !!","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputB");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputC");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>