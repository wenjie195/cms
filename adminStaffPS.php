<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Payment.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $productDetails = getProduct($conn);
// $productDetails = getPayment($conn, " ORDER BY date_created DESC ");
$productDetails = getPayment($conn, " WHERE fullname != 'Fullname' ORDER BY date_created DESC ");
// $productDetails = getUser($conn, "WHERE user_type = 1 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Staff Payslip | CMS" />
    <title>Staff Payslip | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">

    <h1 class="h1-title open">Staff Payslip</h1>

    <a href='uploadExcel.php'><div class="blue-btn width175">Issue Payslip</div></a>

    <div class="clear"></div>

  <!-- <div class="width100"> -->
  <div class="big-four-input-container">
    <form method='post' action='adminStaffPSSessionSearch.php' target="_blank"> 
      <div class="input50-div">
          <p class="input-top-p">Month</p>
          <select class="clean tele-input" type="text" id="month" name="month" required>
            <option value="" name=" ">Please Select a Month</option>
            <option value="Jan" name="Jan">Jan</option>
            <option value="Feb" name="Feb">Feb</option>
            <option value="Mar" name="Mar">Mar</option>
            <option value="Apr" name="Apr">Apr</option>
            <option value="May" name="May">May</option>
            <option value="Jun" name="Jun">Jun</option>
            <option value="Jul" name="Jul">Jul</option>
            <option value="Aug" name="Aug">Aug</option>
            <option value="Sep" name="Sep">Sep</option>
            <option value="Oct" name="Oct">Oct</option>
            <option value="Nov" name="Nov">Nov</option>
            <option value="Dec" name="Dec">Dec</option>
          </select>
        </div>

        <div class="input50-div second-input50">
          <p class="input-top-p">Year</p>
          <input type="text" placeholder="Key in a year" class="clean tele-input" name='years' id="years" required>
        </div>
		<div class="clear"></div>
        <div class="input50-div">
          <button class="blue-btn width175" name="submit">Search</button>
        </div>
    </form>         
  </div>

    <div class="big-four-input-container">
      <div class="input50-div">
        <p class="input-top-p">Staff Name</p>
        <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Staff Name" class="tele-four-input tele-input clean">
      </div>

      <div class="input50-div second-input50">
        <p class="input-top-p">IC Number</p>
        <input type="text" id="myInputB" onkeyup="myFunctionB()" placeholder="IC Number" class="tele-four-input tele-input clean">
      </div>
    </div>

    <div class="width100 shipping-div2">
  
    <div class="overflow-scroll-div">
        <table class="shipping-table" id="myTable">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Staff Name</th>
                    <th>Designation</th>
                    <th>Month / Year</th>
                    <!-- <th>Date</th> -->
                    <th>View</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                if($productDetails)
                {   
                    for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $productDetails[$cnt]->getFullname();?></td>
                            <td><?php echo $productDetails[$cnt]->getDesignation();?></td>
                            <td><?php echo $productDetails[$cnt]->getMonth();?> / <?php echo $productDetails[$cnt]->getYear();?></td>
                            <!-- <td><?php echo $productDetails[$cnt]->getDateCreated();?></td> -->
                            <td>
                              <form action="adminPayslipView.php" method="POST">
                                  <button class="clean hover1 img-btn" type="submit" name="payment_uid" value="<?php echo $productDetails[$cnt]->getUid();?>">
                                      <img src="img/view.png" class="width100 hover1a">
                                      <img src="img/view2.png" class="width100 hover1b" >
                                  </button>
                              </form>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
            </tbody>
        </table>
    </div>
    
    <div class="clear"></div>

</div>

<style>
.payslip-li{
	color:#264a9c;
	background-color:white;}
.payslip-li .hover1a{
	display:none;}
.payslip-li .hover1b{
	display:block;}
</style>

<?php unset($_SESSION['quotation_session']); unset($_SESSION['invoice_session']); unset($_SESSION['receipt_session']); unset($_SESSION['url']);?>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New Staff Added !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Password Updated !"; 
        }
        // elseif($_GET['type'] == 3)
        // {
        //     $messageType = "Incorrect Password";
        // }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputB");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputC");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>