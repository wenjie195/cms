<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/AddOnProduct.php';
require_once dirname(__FILE__) . '/classes/Offer.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $productDetails = getProduct($conn);
$productDetails = getOffer($conn);
// $productDetails = getUser($conn, "WHERE user_type = 1 ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Letter | CMS" />
    <title>Letter | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">

    <h1 class="h1-title open">Letter</h1>

    
      <a href='adminLetterOfferAdd.php'><div class="blue-btn width175">Issue An Offer Letter</div></a>
    

    <div class="clear"></div>

    <div class="big-four-input-container">
      <div class="input50-div">
        <p class="input-top-p">Name</p>
        <input type="text" id="myInput" onkeyup="myFunction()" placeholder="Name" class="tele-four-input tele-input clean">
      </div>

      <div class="input50-div second-input50">
        <p class="input-top-p">IC Number</p>
        <input type="text" id="myInputB" onkeyup="myFunctionB()" placeholder="IC Number" class="tele-four-input tele-input clean">
      </div>

      <!-- <div class="three-input-div">
        <p class="input-top-p">Product Code</p>
        <input type="text" id="myInputC" onkeyup="myFunctionC()" placeholder="Product Code" class="tele-four-input tele-input clean">
      </div> -->
	</div>

    <div class="width100 shipping-div2">
  
    <div class="overflow-scroll-div">
        <table class="shipping-table" id="myTable">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Name</th>
                    <th>IC No</th>
                    <th>Position</th>
                    <th>Offer Letter</th>
                    <th>Confirmation Date</th>
                    <th>Confirmation Letter</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                if($productDetails)
                {   
                    for($cnt = 0;$cnt < count($productDetails) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $productDetails[$cnt]->getName();?></td>
                            <td><?php echo $productDetails[$cnt]->getIcno();?></td>
                            <td><?php echo $productDetails[$cnt]->getPosition();?></td>

                            <td>
                                <form action="adminLetterOfferView.php" method="POST">
                                <!-- <form action="#" method="POST"> -->
                                    <button class="clean hover1 img-btn" type="submit" name="letter_uid" value="<?php echo $productDetails[$cnt]->getUid();?>">
                                        <img src="img/view.png" class="width100 hover1a">
                                        <img src="img/view2.png" class="width100 hover1b" >
                                    </button>
                                </form>
                            </td>

                            <td>
                              <?php 
                                $confirmDate = $productDetails[$cnt]->getConfirmDate();
                                if(!$confirmDate)
                                {}
                                else
                                {
                                  echo $confirmDate; 
                                }
                              ?>

                              <?php 
                                $respond = $productDetails[$cnt]->getStatus();
                                if($respond == 'Reject' || $respond =='Confirm')
                                {}
                                else
                                {
                                ?>
                                
                                <form action="utilities/adminOfferLetterDateFunction.php" method="POST">
                                  <input class="clean date-input" type="date" placeholder="Display Date" id="date" name="date">    
                                  <button class="clean blue-btn" type="submit" name="letter_uid" value="<?php echo $productDetails[$cnt]->getUid();?>">
                                    Submit
                                  </button>
                                </form> 

                                <?php
                                }
                              ?>

                              <!-- <form action="utilities/adminOfferLetterDateFunction.php" method="POST">
                                <input class="clean date-input" type="date" placeholder="Display Date" id="date" name="date">    
                                <button class="clean blue-btn" type="submit" name="letter_uid" value="<?php //echo $productDetails[$cnt]->getUid();?>">
                                  Submit
                                </button>
                              </form>   -->

                            </td>

                            <td>
                                <form action="adminLetterConfirmationView.php" method="POST">
                                <!-- <form action="#" method="POST"> -->
                                    <button class="clean hover1 img-btn" type="submit" name="letter_uid" value="<?php echo $productDetails[$cnt]->getUid();?>">
                                        <img src="img/view.png" class="width100 hover1a" alt="View" title="View"  >
                                        <img src="img/view2.png" class="width100 hover1b" alt="View" title="View" >
                                    </button>
                                </form>
                            </td>

                            <td>
                              <?php 
                                $status = $productDetails[$cnt]->getStatus();
                                if($status == 'Confirm')
                                {
                                  echo $status;
                                }
                                elseif($status == 'Pending')
                                {
                                ?>
                                  <form action="utilities/adminOfferLetterConfirmFunction.php" method="POST" style="display:inline-block; margin-right:5px;">
                                    <button class="clean hover1 img-btn" type="submit" name="letter_uid" value="<?php echo $productDetails[$cnt]->getUid();?>">
                                        <img src="img/tick0.png" class="width100 hover1a" alt="Approve" title="Approve" >
                                        <img src="img/tick2.png" class="width100 hover1b" alt="Approve" title="Approve">
                                    </button>
                                  </form>

                                  <form action="utilities/adminOfferLetterRejectFunction.php" method="POST" style="display:inline-block; ">
                                    <button class="clean hover1 img-btn" type="submit" name="letter_uid" value="<?php echo $productDetails[$cnt]->getUid();?>">
                                        <img src="img/close.png" class="width100 hover1a" alt="Reject" title="Reject" >
                                        <img src="img/close2.png" class="width100 hover1b" alt="Reject" title="Reject">
                                    </button>
                                  </form>
                                <?php
                                }
                                else
                                {
                                  echo $status;
                                }
                              ?>
                            </td>

                        </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
            </tbody>
        </table>
    </div>
    
    <div class="clear"></div>

</div>

<style>
.offer-li{
	color:#264a9c;
	background-color:white;}
.offer-li .hover1a{
	display:none;}
.offer-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "New Staff Added !"; 
        }
        // elseif($_GET['type'] == 2)
        // {
        //     $messageType = "Please Register"; 
        // }
        // elseif($_GET['type'] == 3)
        // {
        //     $messageType = "Incorrect Password";
        // }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionB() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputB");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[2];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

<script>
function myFunctionC() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInputC");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[3];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>