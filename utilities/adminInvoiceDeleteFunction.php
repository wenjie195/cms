<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Invoice.php';
require_once dirname(__FILE__) . '/../classes/InvoiceDetails.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     echo $quotationName = rewrite($_POST['quotation_uid']);

     $quotationRows = getInvoice($conn, " WHERE name = ? ", array("name") ,array($quotationName),"s");
     $quotationUid = $quotationRows[0]->getUid();

     $quotationStatus = "Delete";

     if($quotationRows)
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($quotationStatus)
          {
               array_push($tableName,"status");
               array_push($tableValue,$quotationStatus);
               $stringType .=  "s";
          }
          array_push($tableValue,$quotationUid);
          $stringType .=  "s";
          $passwordUpdated = updateDynamicData($conn,"invoice"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($passwordUpdated)
          {
               // header('Location: ../adminQuotationAll.php');

               $tableName = array();
               $tableValue =  array();
               $stringType =  "";
               //echo "save to database";
               if($quotationStatus)
               {
                    array_push($tableName,"status");
                    array_push($tableValue,$quotationStatus);
                    $stringType .=  "s";
               }
               array_push($tableValue,$quotationName);
               $stringType .=  "s";
               $passwordUpdated = updateDynamicData($conn,"invoice_details"," WHERE quotation_uid = ? ",$tableName,$tableValue,$stringType);
               if($passwordUpdated)
               {
                    header('Location: ../adminInvoiceAll.php');
               }
               else
               {
                    echo "fail (update invoice status)";
               }

          }
          else
          {
               echo "fail (update invoice status)";
          }
     }
     else
     {
          echo "fail 2";
     }

}
else 
{
     header('Location: ../index.php');
}
?>