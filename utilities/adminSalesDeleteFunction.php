<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/../classes/Payment.php';
// require_once dirname(__FILE__) . '/../classes/Receipt.php';
require_once dirname(__FILE__) . '/../classes/Sales.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $salesUid = rewrite($_POST['sales_uid']);
     $updateStatus = "Delete";

     $salesDetails = getSales($conn," WHERE uid = ? ",array("uid"),array($salesUid),"s");    

     if($salesDetails)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";

          if($updateStatus)
          {
               array_push($tableName,"status");
               array_push($tableValue,$updateStatus);
               $stringType .=  "s";
          }

          array_push($tableValue,$salesUid);
          $stringType .=  "s";
          $passwordUpdated = updateDynamicData($conn,"sales"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($passwordUpdated)
          {
               header('Location: ../adminSalesReport.php');
          }
          else
          {
               echo "FAIL";
          }
     }
     else
     {
          echo "ERROR";
     }

}
else 
{
     header('Location: ../index.php');
}
?>