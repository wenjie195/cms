<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = rewrite($_POST['user_uid']);

     $editPassword_new  = $_POST['new_password'];
     $editPassword_reenter  = $_POST['retype_new_password'];

     // $user = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
     // $userDetails = $user[0];

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $uid."<br>";
     // echo $editPassword_current."<br>";

     if(strlen($editPassword_new) >= 6 && strlen($editPassword_reenter) >= 6 )
     {
          if($editPassword_new == $editPassword_reenter)
          {
               $password = hash('sha256',$editPassword_new);
               $salt = substr(sha1(mt_rand()), 0, 100);
               $finalPassword = hash('sha256', $salt.$password);

               $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",array("password","salt"),array($finalPassword,$salt,$uid),"sss");
               if($passwordUpdated)
               {
                    $_SESSION['messageType'] = 1;
                    header( "Location: ../adminDashboard.php?type=2" );
               }
               else 
               {
                    echo "<script>alert('Fail to update password !!');window.location='../adminDashboard.php'</script>";
               }
          }
          else 
          {
               echo "<script>alert('new password must be same with retype new password !!');window.location='../adminDashboard.php'</script>";
          }
     }
     else 
     {
          echo "<script>alert('password length must more than 5 !!');window.location='../adminDashboard.php'</script>";
     }
   
}
else 
{
    header('Location: ../index.php');
}
?>