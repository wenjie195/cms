<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Name.php';
// require_once dirname(__FILE__) . '/../classes/Category.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $supplierUid = rewrite($_POST['supplier_uid']);

     $supplierName = rewrite($_POST['supplier_name']);
     $phone = rewrite($_POST['phone']);
     $address = ($_POST['address']);
     $remark = ($_POST['remark']);

     //   FOR DEBUGGING 
     // echo "<br>";
     // echo $register_uid."<br>";

     $supplierDetails = getName($conn," WHERE uid = ? ",array("uid"),array($_POST['supplier_uid']),"s");
     if($supplierDetails)
     {
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
  
          if($supplierName)
          {
              array_push($tableName,"name");
              array_push($tableValue,$supplierName);
              $stringType .=  "s";
          }
          if($phone)
          {
              array_push($tableName,"phone");
              array_push($tableValue,$phone);
              $stringType .=  "s";
          }
          if($address)
          {
              array_push($tableName,"address");
              array_push($tableValue,$address);
              $stringType .=  "s";
          }
          if($remark)
          {
              array_push($tableName,"remark");
              array_push($tableValue,$remark);
              $stringType .=  "s";
          }
          array_push($tableValue,$supplierUid);
          $stringType .=  "s";
          $passwordUpdated = updateDynamicData($conn,"name"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($passwordUpdated)
          {        
               // header('Location: ../adminAddSupplierName.php');
               $_SESSION['messageType'] = 1;
               header('Location: ../adminAddSupplierName.php?type=1');
          }
          else
          {
          //   echo "unable to update details !!";
            $_SESSION['messageType'] = 1;
            header('Location: ../adminAddSupplierName.php?type=2');
          }
     }
     else
     {
          echo "no data found !! pls recheck";
     }

     // $existingName = $allName[0];

     // if (!$existingName)
     // {
     //      // if(addCategory($conn,$uid,$supplierName,$status))
     //      if(addCategory($conn,$uid,$supplierName,$phone,$address,$remark,$status))
     //      {
     //           header('Location: ../adminAddSupplierName.php');
     //           // echo "success";
     //      }
     //      else
     //      {
     //           echo "fail";
     //      }
     // }
     // else
     // {
     //      echo "supplier name existed !! pls recheck";
     // }
}
else 
{
     header('Location: ../index.php');
}
?>