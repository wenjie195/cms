<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/../classes/LeaveStatus.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $allUser = getUser($conn);
    
    if ($allUser)
    {
        for ($cnt=0; $cnt <count($allUser) ; $cnt++)
        {
            echo $userUid = $allUser[$cnt]->getUid();
            echo "<br>";
            // echo $userUsername = $allUser[$cnt]->getUsername();
            // echo "<br>";
            echo $userTotalAL = $allUser[$cnt]->getLeaveTotal();
            echo "<br>";
            $renewLeave = $userTotalAL + 1 ;

            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
    
            if($renewLeave)
            {
                array_push($tableName,"leave_total");
                array_push($tableValue,$renewLeave);
                $stringType .=  "s";
            }
            array_push($tableValue,$userUid);
            $stringType .=  "s";
            $passwordUpdated = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
            if($passwordUpdated)
            {
                $_SESSION['messageType'] = 1;
                header('Location: ../adminDashboard.php?type=4');
            }
            else
            {
                echo "FAIL";
            }

        }
    }
    else
    {
        echo "ERROR 2";
    }

}
else 
{
     header('Location: ../index.php');
}
?>