<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

// $timestamp = time();

// function addStaff($conn,$uid,$username,$fullname,$icno,$phone,$address,$emergencyContact,$emergencyPpl,$salary,$allowance,$userType,$epf,$sosco,$lhdn,$finalPassword,$salt)
// {
//      if(insertDynamicData($conn,"user",array("uid","username","fullname","ic_no","phone","address","emergency_contact","emergency_ppl","salary","allowance","user_type","epf","sosco","lhdn","password","salt"),
//           array($uid,$username,$fullname,$icno,$phone,$address,$emergencyContact,$emergencyPpl,$salary,$allowance,$userType,$epf,$sosco,$lhdn,$finalPassword,$salt),"ssssssssssisssss") === null)
//      {
//           echo "gg";
//      }
//      else{    }
//      return true;
// }

// function addStaff($conn,$uid,$username,$fullname,$icno,$phone,$address,$emergencyContact,$emergencyPpl,$salary,$allowance,$userType,$epf,$sosco,$lhdn,$epfNo,$soscoNo,$lhdnNo,$finalPassword,$salt)
// {
//      if(insertDynamicData($conn,"user",array("uid","username","fullname","ic_no","phone","address","emergency_contact","emergency_ppl","salary","allowance","user_type",
//                                                   "epf","sosco","lhdn","epf_no","sosco_no","lhdn_no","password","salt"),
//           array($uid,$username,$fullname,$icno,$phone,$address,$emergencyContact,$emergencyPpl,$salary,$allowance,$userType,$epf,$sosco,$lhdn,$epfNo,$soscoNo,$lhdnNo,$finalPassword,$salt),"ssssssssssissssssss") === null)
//      {
//           echo "gg";
//      }
//      else{    }
//      return true;
// }

function addStaff($conn,$uid,$username,$fullname,$icno,$phone,$address,$emergencyContact,$emergencyPpl,$salary,$allowance,$userType,$epfNo,$finalPassword,$salt,$joinDate)
{
     if(insertDynamicData($conn,"user",array("uid","username","fullname","ic_no","phone","address","emergency_contact","emergency_ppl","salary","allowance","user_type","epf_no","sosco","lhdn","password","salt","join_date"),
          array($uid,$username,$fullname,$icno,$phone,$address,$emergencyContact,$emergencyPpl,$salary,$allowance,$userType,$epfNo,$finalPassword,$salt,$joinDate),"ssssssssssissss") === null)
     {
          echo "gg";
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());

     $username = rewrite($_POST['username']);
     $fullname = rewrite($_POST['fullname']);
     $icno = rewrite($_POST['icno']);
     $phone = rewrite($_POST['phone']);
     $address = rewrite($_POST['address']);
     $emergencyContact = rewrite($_POST['emergency_contact']);
     $emergencyPpl = rewrite($_POST['emergency_ppl']);
     $salary = rewrite($_POST['salary']);
     $allowance = rewrite($_POST['allowance']);

     $epfNo = rewrite($_POST['epf_no']);
     $joinDate = rewrite($_POST['join_date']);

     // $epf = rewrite($_POST['epf']);
     // $sosco = rewrite($_POST['sosco']);
     // $lhdn = rewrite($_POST['lhdn']);
     // $epfNo = rewrite($_POST['epf_no']);
     // $soscoNo = rewrite($_POST['sosco_no']);
     // $lhdnNo = rewrite($_POST['lhdn_no']);

     // $userType = 1;
     // $totalLeave = rewrite($_POST['total_leave']);
     $userType = rewrite($_POST['user_type']);

     $register_password = "123321";
     // $register_password = "111111";
     $password = hash('sha256',$register_password);
     $salt = substr(sha1(mt_rand()), 0, 100);
     $finalPassword = hash('sha256', $salt.$password);

     // //   FOR DEBUGGING 
     // echo "<br>";
     // echo $productName."<br>";
     // echo $category."<br>";

     $fullnameRows = getUser($conn," WHERE fullname = ? ",array("fullname"),array($fullname),"s");
     $existingFullname = $fullnameRows[0];

     $icnoRows = getUser($conn," WHERE ic_no = ? ",array("ic_no"),array($icno),"s");
     $existingIcno = $icnoRows[0];

     if (!$existingFullname && !$existingIcno)
     {
          if(addStaff($conn,$uid,$username,$fullname,$icno,$phone,$address,$emergencyContact,$emergencyPpl,$salary,$allowance,$userType,$epfNo,$finalPassword,$salt,$joinDate))
          {
               // echo "success";
               // header('Location: ../adminDashboard.php');     
               $_SESSION['messageType'] = 1;
               header('Location: ../adminDashboard.php?type=1');
          }
          else
          {
               // echo "fail";
               $_SESSION['messageType'] = 1;
               header('Location: ../adminAddNewStaff.php?type=1');
          }
     }
     else
     {
          // echo "<script>alert('Fullname / IC Number has been registered !');window.location='../adminAddNewStaff.php'</script>";
          $_SESSION['messageType'] = 1;
          header('Location: ../adminAddNewStaff.php?type=2');
     }
}
else 
{
     header('Location: ../index.php');
}
?>