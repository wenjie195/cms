<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/Name.php';
// require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allName = getName($conn);
// $allCategory = getCategory($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Add New Staff | CMS" />
    <title>Add New Staff | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">
	<h1 class="h1-title">Add New Staff</h1> 
    <!-- <form action="utilities/addProductFunction.php" method="POST" enctype="multipart/form-data"> -->
    <!-- <form action="utilities/adminAddNewStaffFunction.php" method="POST" enctype="multipart/form-data"> -->
    <!-- <form action="utilities/adminAddNewStaffFunction.php" method="POST"> -->
    <form action="utilities/adminStaffAddNewFunction.php" method="POST">

        <div class="input50-div">
            <p class="input-title-p">Username</p>
            <input class="clean tele-input" type="text" placeholder="Username" id="username" name="username" required>        
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Fullname</p>
            <input class="clean tele-input" type="text" placeholder="Fullname" id="fullname" name="fullname" required>       
        </div> 

        <div class="input50-div">
            <p class="input-title-p">IC Number</p>
            <input class="clean tele-input"  type="text" placeholder="IC Number" id="icno" name="icno" required>        
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Phone</p>
            <input class="clean tele-input"  type="text" placeholder="Phone" id="phone" name="phone" required>     
        </div> 

        <div class="clear"></div>

        <div class="width100">
            <p class="input-title-p">Address</p>
            <textarea  type="text" class="clean tele-input textarea-min-height" placeholder="Address" id="address" name="address"></textarea> 
        </div> 

        <div class="clear"></div>

        <div class="input50-div">
            <p class="input-title-p">Emergency Contact</p>
            <input class="clean tele-input" type="text" placeholder="Emergency Contact" id="emergency_contact" name="emergency_contact">        
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Emergency People Person</p>
            <input class="clean tele-input" type="text" placeholder="Emergency People Person" id="emergency_ppl" name="emergency_ppl">         
        </div> 

        <div class="clear"></div>

        <div class="input50-div">
            <p class="input-title-p">Salary</p>     
            <input class="clean tele-input"  type="text" placeholder="Salary" id="salary" name="salary"> 
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Allowance</p>     
            <input class="clean tele-input"  type="text" placeholder="Allowance" id="allowance" name="allowance">      
        </div> 

        <div class="clear"></div>

        <div class="input50-div">
            <!-- <p class="input-title-p">EPF</p>    -->
            <p class="input-title-p">EPF Number</p>      
            <!-- <input class="clean tele-input"  type="text" placeholder="EPF" id="epf" name="epf">  -->
            <input class="clean tele-input"  type="text" placeholder="EPF Number" id="epf_no" name="epf_no"> 
        </div> 

        <div class="input50-div second-input50">  
            <p class="input-title-p">Date Of Join</p>     
            <input class="clean tele-input"  type="text" placeholder="Date Of Join" id="join_date" name="join_date">  
        </div> 

        <div class="clear"></div>

        <!-- <div class="input50-div second-input50">  
            <p class="input-title-p">Sosco</p>     
            <input class="clean tele-input"  type="text" placeholder="Sosco" id="sosco" name="sosco"> 
        </div> 

        <div class="clear"></div>

        <div class="input50-div">
            <p class="input-title-p">LHDN</p>     
            <input class="clean tele-input"  type="text" placeholder="LHDN" id="lhdn" name="lhdn"> 
        </div>  -->

        <!-- <div class="input50-div second-input50">  
            <p class="input-title-p">Annual Leave (Days)</p>     
            <input class="clean tele-input"  type="number" placeholder="Leave (Total Days)" id="total_leave" name="total_leave" required>  
        </div>  -->

        <!-- <div class="clear"></div> -->

        <!-- <div class="width100"> -->
        <!-- <div class="input50-div second-input50">   -->
        <div class="input50-div">
            <p class="input-title-p">Account Type</p>     
            <select class="clean tele-input" type="text" id="user_type" name="user_type" required>
                <option value="" name=" ">PLEASE SELECT ACCOUNT TYPE</option>
                <option value="0" name="0">Admin</option>
                <option value="1" name="1">Normal User</option>
                <option value="2" name="2">Marketing</option>
            </select> 
        </div> 

        <!-- <div class="input50-div">
            <p class="input-title-p">EPF ID</p>     
            <input class="clean tele-input"  type="text" placeholder="EPF ID" id="epf_no" name="epf_no">     
        </div> 

        <div class="input50-div second-input50">  
            <p class="input-title-p">EPF</p>     
            <input class="clean tele-input"  type="text" placeholder="EPF" id="epf" name="epf"> 
        </div> 

        <div class="clear"></div>

        <div class="input50-div">
            <p class="input-title-p">Sosco ID</p>     
            <input class="clean tele-input"  type="text" placeholder="Sosco ID" id="sosco_no" name="sosco_no"> 
        </div> 

        <div class="input50-div second-input50">  
            <p class="input-title-p">Sosco</p>     
            <input class="clean tele-input"  type="text" placeholder="Sosco" id="sosco" name="sosco"> 
        </div> 

        <div class="clear"></div>

        <div class="input50-div">
            <p class="input-title-p">LHDN ID</p>     
            <input class="clean tele-input"  type="text" placeholder="LHDN ID" id="lhdn_no" name="lhdn_no"> 
        </div> 

        <div class="input50-div second-input50">  
            <p class="input-title-p">LHDN</p>     
            <input class="clean tele-input"  type="text" placeholder="LHDN" id="lhdn" name="lhdn"> 
        </div>  -->

        <div class="clear"></div>

        <button class="clean blue-btn red-btn margin-top30 fix300-btn margin-bottom30" name="submit">Submit</button>

        <div class="clear"></div>
    </form>
</div>

<style>
.account-li{
	color:#264a9c;
	background-color:white;}
.account-li .hover1a{
	display:none;}
.account-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Fail To Add New Staff !!"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Fullname / IC Number has been registered !"; 
        }
        // elseif($_GET['type'] == 3)
        // {
        //     $messageType = "Incorrect Password";
        // }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>