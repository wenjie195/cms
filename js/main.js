function putNoticeJavascript(header,text)
{
    document.getElementById('noticeTitle').innerHTML = header;
    document.getElementById('noticeMessage').innerHTML = text;

    // Get the modal
    var modal = document.getElementById('notice-modal');

    // Get the <span> element that closes the modal
    var span = document.getElementById('close-notice');

    modal.style.display = 'block';


    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = 'none';
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = 'none';
        }
    }
}
