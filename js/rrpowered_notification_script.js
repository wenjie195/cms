$(function() {
	$(".open").click(function() {
		/*Set the timeout timer, 5000 milliseconds equals 5 seconds*/
		var timer = 5000, timeoutId;
		var x = document.getElementById("myAudio"); 
		playAudio();
		/*Set the selector for the notification alert box*/
		var notiAlert = $(".rrpowered_noti");
		
		/*Call the function to show the notification alert box*/
		showAlert();
		
		/*Call the function to start the timer to close the notification alert box*/
		startTimer();

        /*The function to show notification alert box*/
		function showAlert() {
			$(".rrpowered_notification").html('<div class="rrpowered_noti noti_clearfix"><div class="rrpowered_noti_content noti_left"> <p>"<script><?php echo $timer ?></script>"1 Good is being collected. <br><b>Jack</b></p></div><div class="icon_close noti_right" id="close">X</div></div>');
		}

        /*The function to start the timeout timer*/
		function startTimer() {
			timeoutId = setTimeout(function() {
				$(".rrpowered_noti").hide();
			}, timer);
		}
		function playAudio() { 
		  x.play(); 
		} 
        /*The function to stop the timeout timer*/
		function stopTimer() {
			clearTimeout(timeoutId);
		}

        /*Call the stopTimer function on mouse enter and call the startTimer function on mouse leave*/
		$(".rrpowered_noti").mouseenter(stopTimer).mouseleave(startTimer);

        /*Close the notification alert box when close button is clicked*/
		$("#close").click(function() {
			$(".rrpowered_noti").hide();
		});
	});
}); 