<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Name.php';
// require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allCategory = getCategory($conn);
$allName = getName($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Add Supplier Name | CMS" />
    <title>Add Supplier Name | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">
	<h1 class="h1-title">Edit Supplier Details</h1> 

    <?php
    // echo $_POST['user_uid'];
    if(isset($_POST['user_uid']))
    {
    $conn = connDB();
    $supplierDetails = getName($conn,"WHERE uid = ? ", array("uid") ,array($_POST['user_uid']),"s");
    ?>

    <form action="utilities/markertingAddSupplierNameEditFunction.php" method="POST">

        <div class="input50-div">
            <p class="input-title-p">Supplier Name</p>
            <input class="clean tele-input" type="text"  placeholder="Supplier Name" value="<?php echo $supplierDetails[0]->getName();?>" id="supplier_name" name="supplier_name" required>        
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Phone</p>
            <input class="clean tele-input" type="text" placeholder="Phone" value="<?php echo $supplierDetails[0]->getPhone();?>" id="phone" name="phone">       
        </div> 

        <div class="clear"></div>

        <div class="width100">
            <p class="input-title-p">Address</p>
            <textarea  type="text" class="clean tele-input textarea-min-height" placeholder="Address" id="address" name="address"><?php echo $supplierDetails[0]->getAddress();?></textarea> 
        </div> 

        <div class="clear"></div>

        <div class="width100">
            <p class="input-title-p">Remark</p>    
            <textarea type="text" class="clean tele-input" rows="10" cols="80" placeholder="Remark" id="remark" name="remark"><?php echo $supplierDetails[0]->getRemark();?></textarea>  
        </div> 

        <div class="clear"></div>

        <input class="clean tele-input" type="hidden" value="<?php echo $_POST['user_uid'];?>" id="supplier_uid" name="supplier_uid" readonly>    

        <!-- <button class="clean red-btn fix300-btn align-left" name="submit">Submit</button> -->
        <button class="clean red-btn margin-top30 fix300-btn" name="submit">Submit</button>

    </form>

    <?php
    }
    ?>

    <div class="clear"></div>

</div>

<!-- <style>
.telemarketer-li{
	color:#264a9c;
	background-color:white;}
.telemarketer-li .hover1a{
	display:none;}
.telemarketer-li .hover1b{
	display:block;}
</style> -->

<?php include 'js.php'; ?>

</body>
</html>