<div class="side-bar red-bg no-print">
	<div class="width100 text-center">
    	<img src="img/gp.png" class="logo-img" alt="logo" title="logo">
    </div>
	<ul class="sidebar-ul">
    	<a href="userDashboard.php">
        	<li class="sidebar-li dashboard-li hover1">
            	<img src="img/dashboard1.png" class="hover1a" alt="Dashboard" title="Dashboard">
                <img src="img/dashboard2.png" class="hover1b" alt="Dashboard" title="Dashboard">
                 <p>Dashboard</p>
            </li>
        </a>

        <a href="userLeaveAll.php">
        	<li class="sidebar-li leave-li hover1">
            	<img src="img/leave.png" class="hover1a" alt="Leave" title="Leave">
                <img src="img/leave2.png" class="hover1b" alt="Leave" title="Leave ">
                 <p>Leave</p>
            </li>
        </a>

        <a href="marketingQuotationAll.php">
        	<li class="sidebar-li quotation-li hover1">
            	<img src="img/quotation.png" class="hover1a" alt="Quotation" title="Quotation">
                <img src="img/quotation2.png" class="hover1b" alt="Quotation" title="Quotation">
                 <p>Quotation</p>
            </li>
        </a>

        <a href="logout.php">
        	<li class="sidebar-li hover1">
            	<img src="img/logout1.png" class="hover1a" alt="Logout" title="Logout">
                <img src="img/logout2.png" class="hover1b" alt="Logout" title="Logout">  
            	 <p>Logout</p>
            </li>
       </a>
    </ul>
</div>

<header id="header" class="header header--fixed same-padding header1 menu-color no-print" role="banner">
    <img src="img/gp-white.png" class="mobile-logo" alt="Logo" title="Logo">
    <div id="dl-menu" class="dl-menuwrapper">
        <button class="dl-trigger">Open Menu</button>
        <ul class="dl-menu">
            <a href="userDashboard.php">
                <li class="sidebar-li dashboard-li hover1">
                    <img src="img/dashboard1.png" class="hover1a" alt="Dashboard" title="Dashboard">
                    <img src="img/dashboard2.png" class="hover1b" alt="Dashboard" title="Dashboard">
                    <p>Dashboard</p>
                </li>
            </a>
            <a href="userLeaveAll.php">
                <li class="sidebar-li leave-li hover1">
                    <img src="img/leave.png" class="hover1a" alt="Leave" title="Leave">
                    <img src="img/leave2.png" class="hover1b" alt="Leave" title="Leave ">
                    <p>Leave</p>
                </li>
            </a>
            <a href="marketingQuotationAll.php">
                <li class="sidebar-li quotation-li hover1">
                    <img src="img/quotation.png" class="hover1a" alt="Quotation" title="Quotation">
                    <img src="img/quotation2.png" class="hover1b" alt="Quotation" title="Quotation">
                    <p>Quotation</p>
                </li>
            </a>
            <a href="logout.php">
                <li class="sidebar-li hover1">
                    <img src="img/logout1.png" class="hover1a" alt="Logout" title="Logout">
                    <img src="img/logout2.png" class="hover1b" alt="Logout" title="Logout">  
                    <p>Logout</p>
                </li>
            </a>
        </ul>
    </div>
</header>