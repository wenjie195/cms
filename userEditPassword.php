<?php
// require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Edit Password | CMS" />
    <title>Edit Password | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">


<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'userSidebar.php'; ?>
<div class="next-to-sidebar">

    <h1 class="h1-title open">Edit Password</h1>

    <div class="clear"></div>

    <form action="utilities/userEditPasswordFunction.php" method="POST">

        <div class="input50-div">
        	<p class="input-title-p">Current Password</p>
            <div class="password-input-div">
                <input class="clean tele-input password-input"  type="password" placeholder="Current Password" id="current_password" name="current_password" required>  
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionA()" alt="View Password" title="View Password">
            </div>   
        </div>

        <div class="clear"></div>

        <div class="input50-div">
        	<p class="input-title-p">New Password</p>
            <div class="password-input-div">
                <input class="clean tele-input password-input"  type="password" placeholder="New Password" id="new_password" name="new_password" required>  
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionB()" alt="View Password" title="View Password">
            </div>   
        </div>

        <div class="input50-div second-input50">
        	<p class="input-title-p">Retype New Password</p>
            <div class="password-input-div">
                <input class="clean tele-input password-input"  type="password" placeholder="Retype New Password" id="retype_new_password" name="retype_new_password" required>  
                <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionC()" alt="View Password" title="View Password">
            </div>
        </div>      

        <div class="clear"></div>

        <button class="clean red-btn margin-top30 fix300-btn" name="submit">Submit</button>

    </form>

</div>

<style>
.edit-li{
	color:#264a9c;
	background-color:white;}
.edit-li .hover1a{
	display:none;}
.edit-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<?php 
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Your current password does not match! <br>Please try again.";
        }
        if($_GET['type'] == 2)
        {
            $messageType = "Your new password must be more than 5 characters";
        }
        if($_GET['type'] == 3)
        {
            $messageType = "Password does not match with retype password. <br> Please try again.";
        }
        if($_GET['type'] == 4)
        {
            $messageType = "Server problem. <br>Please try again later.";
        }
        if($_GET['type'] == 5)
        {
            $messageType = "Password successfully updated!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

<script>
function myFunctionA()
{
    var x = document.getElementById("current_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("retype_new_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>

</body>
</html>