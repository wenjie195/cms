-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 06, 2021 at 05:42 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `bill_to` varchar(255) DEFAULT NULL,
  `term` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `uid`, `name`, `bill_to`, `term`, `amount`, `date`, `user_uid`, `status`, `date_created`, `date_updated`) VALUES
(1, 'c384a57c5844ba30a395545c5fe30aef', 'Twiggy (Pulau Tikus) Sdn Bhd1617088514', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '1699', '2020-10-23', NULL, 'Convert', '2021-03-30 07:18:40', '2021-03-30 07:36:08'),
(2, '95be2f94189c96ca69d59d5f1de84c3e', 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '1699', '2020-12-01', NULL, 'Convert', '2021-03-30 07:43:29', '2021-03-31 06:59:30'),
(3, '483f585305a869c02a90e04427565688', 'Twiggy (Pulau Tikus) Sdn Bhd1617090483', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '1699', '2021-01-04', NULL, 'Convert', '2021-03-30 07:48:40', '2021-03-31 06:59:33'),
(4, 'f886979eec6cb8608073e67c06ba2ad5', 'Twiggy (Pulau Tikus) Sdn Bhd1617090582', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '1699', '2021-02-15', NULL, 'Convert', '2021-03-30 07:50:18', '2021-03-31 06:59:35'),
(5, '1b2b010b8e1407df29bbc5aae05c4024', '56 Hair Saloon1617091356', '56 Hair Saloon', 'Campaign', '588', '2020-10-23', NULL, 'Convert', '2021-03-30 08:06:11', '2021-03-31 06:59:37'),
(6, '058e75651b6bbec90ce11951941d3250', '56 Hair Saloon1617091634', '56 Hair Saloon', 'Campaign', '588', '2020-12-01', NULL, 'Convert', '2021-03-30 08:07:49', '2021-03-31 06:59:39'),
(7, 'cdf7d892cd645f2867a91c9850e46156', '56 Hair Saloon1617091707', '56 Hair Saloon', 'Campaign', '588', '2021-01-04', NULL, 'Convert', '2021-03-30 08:08:52', '2021-03-31 06:59:41'),
(8, 'c5ed99efdcc203e82d1309e37647e41a', '56 Hair Saloon1617091772', '56 Hair Saloon', 'Campaign', '588', '2021-02-15', NULL, 'Convert', '2021-03-30 08:09:53', '2021-03-31 06:59:43');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_details`
--

CREATE TABLE `invoice_details` (
  `id` bigint(20) NOT NULL,
  `quotation_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `unit_price` varchar(255) DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `total` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_details`
--

INSERT INTO `invoice_details` (`id`, `quotation_uid`, `uid`, `product_name`, `quantity`, `unit_price`, `uom`, `description`, `total`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Twiggy (Pulau Tikus) Sdn Bhd1617088514', '3866fd50c9db80b7708720ca9d84bb2e', 'Social Media Digital Marketing Packages', '1', '1699', '1', NULL, '1699', 'Delete', '2021-03-30 07:18:40', '2021-03-30 08:56:56'),
(2, 'Twiggy (Pulau Tikus) Sdn Bhd1617088514', '6fdf5061f92968f2586a19f6678c236c', 'Basic Marketing Package (Monthly)', '1', '1699', '', NULL, '1699', 'Convert', '2021-03-30 07:35:05', '2021-03-30 07:36:08'),
(3, 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', 'b94cc785632264da4804d53239af4616', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Convert', '2021-03-30 07:43:29', '2021-03-31 06:59:30'),
(4, 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', '814f6e545033f307ad307974247c9214', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Convert', '2021-03-30 07:43:29', '2021-03-31 06:59:30'),
(5, 'Twiggy (Pulau Tikus) Sdn Bhd1617090483', 'a0a53b0c6868e04024ccf97e4b674f6a', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Convert', '2021-03-30 07:48:40', '2021-03-31 06:59:33'),
(6, 'Twiggy (Pulau Tikus) Sdn Bhd1617090582', 'f4046634d0610b5db9f90d999107d574', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Convert', '2021-03-30 07:50:18', '2021-03-31 06:59:35'),
(7, '56 Hair Saloon1617091356', 'ac403396b32846f68d71579a53573b7e', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Convert', '2021-03-30 08:06:11', '2021-03-31 06:59:37'),
(8, '56 Hair Saloon1617091634', '975be16047152858de48dd0504dddd52', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Convert', '2021-03-30 08:07:49', '2021-03-31 06:59:39'),
(9, '56 Hair Saloon1617091707', 'f8939e53aef609a3e0a7442792453cc9', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Convert', '2021-03-30 08:08:52', '2021-03-31 06:59:41'),
(10, '56 Hair Saloon1617091772', '5c58282dd11155274b4a840268d8eccc', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Convert', '2021-03-30 08:09:53', '2021-03-31 06:59:43');

-- --------------------------------------------------------

--
-- Table structure for table `leave_status`
--

CREATE TABLE `leave_status` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `start_date` varchar(255) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `total_days` varchar(255) DEFAULT NULL,
  `reason` text DEFAULT NULL,
  `doc_one` varchar(255) DEFAULT NULL,
  `doc_two` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `approved_date` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `name`
--

CREATE TABLE `name` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `name`
--

INSERT INTO `name` (`id`, `uid`, `name`, `address`, `phone`, `remark`, `product_code`, `status`, `date_created`, `date_updated`) VALUES
(1, 'e9edf359704a6ce0fb630060072ed368', '56 Hair Saloon', NULL, NULL, NULL, NULL, 'Available', '2021-03-30 05:18:31', '2021-03-30 05:18:31'),
(2, 'c567b15314b01fcef65e156ee6e953d1', 'Twiggy (Pulau Tikus) Sdn Bhd', NULL, NULL, NULL, NULL, 'Available', '2021-03-30 05:18:56', '2021-03-30 05:18:56'),
(3, '4b2f90b2ebf4182ab0c1052a9be66463', 'Keong Bread Enterprise', NULL, NULL, NULL, NULL, 'Available', '2021-03-30 05:24:39', '2021-03-30 05:24:39'),
(4, '366ad908d959699152fa8a5882b0eef6', 'Wee Ming Seow', NULL, NULL, NULL, NULL, 'Available', '2021-03-30 05:25:56', '2021-03-30 05:25:56'),
(5, '8d775814c0e173dbe08b7d4fdce8e724', 'Zip Tech Enterprise', NULL, NULL, NULL, NULL, 'Available', '2021-03-30 05:26:14', '2021-03-30 05:26:14'),
(6, 'cd36f9d6debc009382d6553bc3e59ada', 'Kang Wai Cool Enterprise', NULL, NULL, NULL, NULL, 'Available', '2021-03-30 05:26:27', '2021-03-30 05:26:27'),
(7, '2ae990493681f110998c80c3eee2a13d', 'Dragon Wood Enterprise', NULL, NULL, NULL, NULL, 'Available', '2021-03-30 05:26:50', '2021-03-30 05:26:50'),
(8, 'ff4d59776529f52a67de762f07a7b1f5', 'Saik Yew Chong', NULL, NULL, NULL, NULL, 'Available', '2021-03-30 05:27:04', '2021-03-30 05:27:04'),
(9, '89461700e752d128ee577897f2852b04', 'SM Wealth Dreamday', NULL, NULL, NULL, NULL, 'Available', '2021-03-30 05:27:28', '2021-03-30 05:27:28'),
(10, 'a6efb78a80eb7ec3896b06756f73987e', 'Wayne Teh Cheah Wen', NULL, NULL, NULL, NULL, 'Available', '2021-03-30 05:28:18', '2021-03-30 05:28:18'),
(11, '0d8d8aed1f8a9ee37a2d5c369ed5ee14', 'Yeap Chai Hing', NULL, NULL, NULL, NULL, 'Available', '2021-03-30 05:28:42', '2021-03-30 05:28:42'),
(12, '147b125b14c4d209bfc883678a90c90f', 'Phuah Pei Yin', NULL, NULL, NULL, NULL, 'Available', '2021-03-30 05:29:02', '2021-03-30 05:29:02'),
(13, 'bedde894badf2a117706bc9f3ca432d2', 'Keith Ooi Jia Sheng', NULL, NULL, NULL, NULL, 'Available', '2021-03-30 05:29:10', '2021-03-30 05:29:10'),
(14, '1c3711be9cb82684ac6987d9ff7b58c0', 'Winnie Ting Wei Ni', NULL, NULL, NULL, NULL, 'Available', '2021-03-30 05:30:22', '2021-03-30 05:30:22');

-- --------------------------------------------------------

--
-- Table structure for table `offer`
--

CREATE TABLE `offer` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `ic_no` varchar(255) DEFAULT NULL,
  `address_one` text DEFAULT NULL,
  `address_two` text DEFAULT NULL,
  `address_three` text DEFAULT NULL,
  `address_four` text DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `position` text DEFAULT NULL,
  `start_date` varchar(255) DEFAULT NULL,
  `working_hrs` varchar(255) DEFAULT NULL,
  `salary` varchar(255) DEFAULT NULL,
  `probation` varchar(255) DEFAULT NULL,
  `job_scope` text DEFAULT NULL,
  `confirm_date` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `ic_no` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `join_date` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `epf_no` varchar(255) DEFAULT NULL,
  `account_no` varchar(255) DEFAULT NULL,
  `income_tax_no` varchar(255) DEFAULT NULL,
  `bank` varchar(255) DEFAULT NULL,
  `basic_pay` varchar(255) DEFAULT NULL,
  `allowance` varchar(255) DEFAULT NULL,
  `commission` varchar(255) DEFAULT NULL,
  `bonus` varchar(255) DEFAULT NULL,
  `overtime_hrs` varchar(255) DEFAULT NULL,
  `overtime_cost` varchar(255) DEFAULT NULL,
  `epf` varchar(255) DEFAULT NULL,
  `perkeso` varchar(255) DEFAULT NULL,
  `eis` varchar(255) DEFAULT NULL,
  `pcb` varchar(255) DEFAULT NULL,
  `unpaid_leave` varchar(255) DEFAULT NULL,
  `salary_after_ul` varchar(255) DEFAULT NULL,
  `epf_comp` varchar(255) DEFAULT NULL,
  `perkeso_comp` varchar(255) DEFAULT NULL,
  `eis_comp` varchar(255) DEFAULT NULL,
  `gross_income` varchar(255) DEFAULT NULL,
  `net_income` varchar(255) DEFAULT NULL,
  `gross_total` varchar(255) DEFAULT NULL,
  `net_month_pay` varchar(255) DEFAULT NULL,
  `al_entitle` varchar(255) DEFAULT NULL,
  `al_applied` varchar(255) DEFAULT NULL,
  `al_balance` varchar(255) DEFAULT NULL,
  `ml_entitle` varchar(255) DEFAULT NULL,
  `ml_applied` varchar(255) DEFAULT NULL,
  `ml_balanced` varchar(255) DEFAULT NULL,
  `month` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `date_updated` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `part_number` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `cost` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `expired_date` varchar(255) DEFAULT NULL,
  `maintenance` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `quotation`
--

CREATE TABLE `quotation` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `bill_to` varchar(255) DEFAULT NULL,
  `term` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quotation`
--

INSERT INTO `quotation` (`id`, `uid`, `name`, `bill_to`, `term`, `amount`, `date`, `user_uid`, `status`, `date_created`, `date_updated`) VALUES
(1, 'c384a57c5844ba30a395545c5fe30aef', 'Twiggy (Pulau Tikus) Sdn Bhd1617088514', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '1699', '2020-10-23', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-03-30 07:17:22', '2021-03-30 07:18:40'),
(2, '95be2f94189c96ca69d59d5f1de84c3e', 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '1699', '2020-12-01', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-03-30 07:41:55', '2021-03-30 07:43:29'),
(3, '483f585305a869c02a90e04427565688', 'Twiggy (Pulau Tikus) Sdn Bhd1617090483', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '1699', '2021-01-04', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-03-30 07:48:15', '2021-03-30 07:48:40'),
(4, 'f886979eec6cb8608073e67c06ba2ad5', 'Twiggy (Pulau Tikus) Sdn Bhd1617090582', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '1699', '2021-02-15', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-03-30 07:49:49', '2021-03-30 07:50:18'),
(5, '1b2b010b8e1407df29bbc5aae05c4024', '56 Hair Saloon1617091356', '56 Hair Saloon', 'Campaign', '588', '2020-10-23', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-03-30 08:04:38', '2021-03-30 08:06:11'),
(6, '058e75651b6bbec90ce11951941d3250', '56 Hair Saloon1617091634', '56 Hair Saloon', 'Campaign', '588', '2020-12-01', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-03-30 08:07:38', '2021-03-30 08:07:49'),
(7, 'cdf7d892cd645f2867a91c9850e46156', '56 Hair Saloon1617091707', '56 Hair Saloon', 'Campaign', '588', '2021-01-04', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-03-30 08:08:37', '2021-03-30 08:08:52'),
(8, 'c5ed99efdcc203e82d1309e37647e41a', '56 Hair Saloon1617091772', '56 Hair Saloon', 'Campaign', '588', '2021-02-15', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-03-30 08:09:39', '2021-03-30 08:09:53');

-- --------------------------------------------------------

--
-- Table structure for table `quotation_details`
--

CREATE TABLE `quotation_details` (
  `id` bigint(20) NOT NULL,
  `quotation_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `unit_price` varchar(255) DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `total` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quotation_details`
--

INSERT INTO `quotation_details` (`id`, `quotation_uid`, `uid`, `product_name`, `quantity`, `unit_price`, `uom`, `description`, `total`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Twiggy (Pulau Tikus) Sdn Bhd1617088514', '3866fd50c9db80b7708720ca9d84bb2e', 'Social Media Digital Marketing Packages', '1', '1699', '1', NULL, '1699', 'Convert', '2021-03-30 07:17:22', '2021-03-30 07:18:40'),
(2, 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', 'b94cc785632264da4804d53239af4616', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Convert', '2021-03-30 07:41:55', '2021-03-30 07:43:29'),
(3, 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', '814f6e545033f307ad307974247c9214', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Convert', '2021-03-30 07:43:04', '2021-03-30 07:43:29'),
(4, 'Twiggy (Pulau Tikus) Sdn Bhd1617090483', 'a0a53b0c6868e04024ccf97e4b674f6a', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Convert', '2021-03-30 07:48:15', '2021-03-30 07:48:40'),
(5, 'Twiggy (Pulau Tikus) Sdn Bhd1617090582', 'f4046634d0610b5db9f90d999107d574', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Convert', '2021-03-30 07:49:49', '2021-03-30 07:50:18'),
(6, '56 Hair Saloon1617091356', 'ac403396b32846f68d71579a53573b7e', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Convert', '2021-03-30 08:04:38', '2021-03-30 08:06:11'),
(7, '56 Hair Saloon1617091634', '975be16047152858de48dd0504dddd52', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Convert', '2021-03-30 08:07:38', '2021-03-30 08:07:49'),
(8, '56 Hair Saloon1617091707', 'f8939e53aef609a3e0a7442792453cc9', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Convert', '2021-03-30 08:08:37', '2021-03-30 08:08:52'),
(9, '56 Hair Saloon1617091772', '5c58282dd11155274b4a840268d8eccc', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Convert', '2021-03-30 08:09:39', '2021-03-30 08:09:53');

-- --------------------------------------------------------

--
-- Table structure for table `receipt`
--

CREATE TABLE `receipt` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `bill_to` varchar(255) DEFAULT NULL,
  `term` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `month` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `receipt`
--

INSERT INTO `receipt` (`id`, `uid`, `name`, `bill_to`, `term`, `amount`, `date`, `month`, `year`, `user_uid`, `status`, `date_created`, `date_updated`) VALUES
(1, 'c384a57c5844ba30a395545c5fe30aef', 'Twiggy (Pulau Tikus) Sdn Bhd1617088514', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '1699', '2020-10-23', 'Nov', '2020', NULL, 'Closed', '2021-03-30 07:36:08', '2021-03-30 07:36:08'),
(2, '95be2f94189c96ca69d59d5f1de84c3e', 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '2662.9', '2020-12-01', 'Dec', '2020', NULL, 'Closed', '2021-03-31 06:59:30', '2021-03-31 06:59:30'),
(3, '483f585305a869c02a90e04427565688', 'Twiggy (Pulau Tikus) Sdn Bhd1617090483', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '2353.21', '2021-01-04', 'Jan', '2020', NULL, 'Closed', '2021-03-31 06:59:33', '2021-03-31 06:59:33'),
(4, 'f886979eec6cb8608073e67c06ba2ad5', 'Twiggy (Pulau Tikus) Sdn Bhd1617090582', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '2067.78', '2021-02-15', 'Feb', '2021', NULL, 'Closed', '2021-03-31 06:59:35', '2021-03-31 06:59:35'),
(5, '1b2b010b8e1407df29bbc5aae05c4024', '56 Hair Saloon1617091356', '56 Hair Saloon', 'Campaign', '588', '2020-10-23', 'Oct', '2020', NULL, 'Closed', '2021-03-31 06:59:37', '2021-03-31 06:59:37'),
(6, '058e75651b6bbec90ce11951941d3250', '56 Hair Saloon1617091634', '56 Hair Saloon', 'Campaign', '909.62', '2020-12-01', 'Dec', '2020', NULL, 'Closed', '2021-03-31 06:59:39', '2021-03-31 06:59:39'),
(7, 'cdf7d892cd645f2867a91c9850e46156', '56 Hair Saloon1617091707', '56 Hair Saloon', 'Campaign', '1656.8', '2021-01-04', 'Jan', '2021', NULL, 'Closed', '2021-03-31 06:59:41', '2021-03-31 06:59:41'),
(8, 'c5ed99efdcc203e82d1309e37647e41a', '56 Hair Saloon1617091772', '56 Hair Saloon', 'Campaign', '1027.61', '2021-02-15', 'Feb', '2021', NULL, 'Closed', '2021-03-31 06:59:43', '2021-03-31 06:59:43');

-- --------------------------------------------------------

--
-- Table structure for table `receipt_details`
--

CREATE TABLE `receipt_details` (
  `id` bigint(20) NOT NULL,
  `quotation_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `unit_price` varchar(255) DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `total` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `receipt_details`
--

INSERT INTO `receipt_details` (`id`, `quotation_uid`, `uid`, `product_name`, `quantity`, `unit_price`, `uom`, `description`, `total`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Twiggy (Pulau Tikus) Sdn Bhd1617088514', '3866fd50c9db80b7708720ca9d84bb2e', 'Social Media Digital Marketing Packages', '1', '1699', '1', NULL, '1699', 'Closed', '2021-03-30 07:36:08', '2021-03-30 07:36:08'),
(2, 'Twiggy (Pulau Tikus) Sdn Bhd1617088514', '6fdf5061f92968f2586a19f6678c236c', 'Basic Marketing Package (Monthly)', '1', '1699', '', NULL, '1699', 'Closed', '2021-03-30 07:36:08', '2021-03-30 07:36:08'),
(3, 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', 'b94cc785632264da4804d53239af4616', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Closed', '2021-03-31 06:59:30', '2021-03-31 06:59:30'),
(4, 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', '814f6e545033f307ad307974247c9214', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Delete', '2021-03-31 06:59:30', '2021-03-31 06:59:30'),
(5, 'Twiggy (Pulau Tikus) Sdn Bhd1617090483', 'a0a53b0c6868e04024ccf97e4b674f6a', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Closed', '2021-03-31 06:59:33', '2021-03-31 06:59:33'),
(6, 'Twiggy (Pulau Tikus) Sdn Bhd1617090582', 'f4046634d0610b5db9f90d999107d574', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Closed', '2021-03-31 06:59:35', '2021-03-31 06:59:35'),
(7, '56 Hair Saloon1617091356', 'ac403396b32846f68d71579a53573b7e', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Closed', '2021-03-31 06:59:37', '2021-03-31 06:59:37'),
(8, '56 Hair Saloon1617091634', '975be16047152858de48dd0504dddd52', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Closed', '2021-03-31 06:59:39', '2021-03-31 06:59:39'),
(9, '56 Hair Saloon1617091707', 'f8939e53aef609a3e0a7442792453cc9', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Closed', '2021-03-31 06:59:41', '2021-03-31 06:59:41'),
(10, '56 Hair Saloon1617091772', '5c58282dd11155274b4a840268d8eccc', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Closed', '2021-03-31 06:59:43', '2021-03-31 06:59:43'),
(11, 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', '411f4b82b241b6f19b370aa95055710d', 'Social Media Ads Boosting Fee (November 2020)', '1', '480.9', '', NULL, '480.9', 'Delete', '2021-03-31 07:02:37', '2021-03-31 07:02:37'),
(12, 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', '28832eb9c4d70b36e5c45d167ac83e25', 'Social Media Ads Boosting Fee (November 2020)', '1', '480.9', '1', NULL, '480.9', 'Closed', '2021-03-31 07:03:02', '2021-03-31 07:03:02'),
(13, '56 Hair Saloon1617091634', '34edf43002f6bfaf260c2439b97b1d09', 'Social Media Ads Boosting Fee (November 2020)', '1', '321.62', '1', NULL, '321.62', 'Closed', '2021-03-31 07:03:47', '2021-03-31 07:03:47'),
(14, 'Twiggy (Pulau Tikus) Sdn Bhd1617090483', 'e5942997b1a8dcf0649f78cde660d3f3', 'Social Media Ads Boosting Fee (December 2020)', '1', '654.21', '1', NULL, '654.21', 'Closed', '2021-03-31 07:04:51', '2021-03-31 07:04:51'),
(15, '56 Hair Saloon1617091707', '611db3c1b1d1c98c21debb2ab277cfd6', 'Social Media Ads Boosting Fee (December 2020)', '1', '1068.8', '1', NULL, '1068.8', 'Closed', '2021-03-31 07:05:43', '2021-03-31 07:05:43'),
(16, 'Twiggy (Pulau Tikus) Sdn Bhd1617090582', 'c6144d7d9cfa8d7d35aa5fea59ac7e09', 'Social Media Ads Boosting Fee (January 2021)', '1', '368.78', '1', NULL, '368.78', 'Closed', '2021-03-31 07:07:02', '2021-03-31 07:07:02'),
(17, '56 Hair Saloon1617091772', '5023a6f127fcfd5cba5f19c1cf802337', 'Social Media Ads Boosting Fee (January 2021)', '1', '439.61', '1', NULL, '439.61', 'Closed', '2021-03-31 07:08:06', '2021-03-31 07:08:06'),
(18, 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', '9d19eb3867030a0921438acc4ed3be82', 'asd', '23', '21', 'can', 'asd\r\ncvb\r\nvbng', '483', 'Pending', '2021-03-31 17:49:28', '2021-03-31 17:49:28');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `sales` varchar(255) DEFAULT NULL,
  `salary` varchar(255) DEFAULT NULL,
  `epf` varchar(255) DEFAULT NULL,
  `socso` varchar(255) DEFAULT NULL,
  `eis` varchar(255) DEFAULT NULL,
  `pcb` varchar(255) DEFAULT NULL,
  `rental` varchar(255) DEFAULT NULL,
  `expenses` varchar(255) DEFAULT NULL,
  `profit` varchar(255) DEFAULT NULL,
  `month` varchar(255) DEFAULT NULL,
  `years` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `ic_no` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `emergency_contact` varchar(255) DEFAULT NULL,
  `emergency_ppl` varchar(255) DEFAULT NULL,
  `salary` varchar(255) DEFAULT NULL,
  `allowance` varchar(255) DEFAULT NULL,
  `leave_total` varchar(255) DEFAULT NULL,
  `leave_applied` varchar(255) DEFAULT NULL,
  `join_date` varchar(255) DEFAULT NULL,
  `epf` varchar(255) DEFAULT NULL,
  `epf_no` varchar(255) DEFAULT NULL,
  `sosco` varchar(255) DEFAULT NULL,
  `sosco_no` varchar(255) DEFAULT NULL,
  `lhdn` varchar(255) DEFAULT NULL,
  `lhdn_no` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `fullname`, `ic_no`, `phone`, `address`, `emergency_contact`, `emergency_ppl`, `salary`, `allowance`, `leave_total`, `leave_applied`, `join_date`, `epf`, `epf_no`, `sosco`, `sosco_no`, `lhdn`, `lhdn_no`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '9ca817799c575c1afd37cccf6a22aa7f', 'admin', 'admin@gmail.com', 'fe8192029d7b39d66fe035ea96623f6ab6f06c961985d12f210b353764b1e90a', 'e74df95cbd449eb12c45c8685694897a6869037f', 'admin', '12312301', '01212312301', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-01-14 06:36:33', '2021-03-19 05:32:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_status`
--
ALTER TABLE `leave_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `name`
--
ALTER TABLE `name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer`
--
ALTER TABLE `offer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation`
--
ALTER TABLE `quotation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation_details`
--
ALTER TABLE `quotation_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt`
--
ALTER TABLE `receipt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_details`
--
ALTER TABLE `receipt_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `invoice_details`
--
ALTER TABLE `invoice_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `leave_status`
--
ALTER TABLE `leave_status`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `name`
--
ALTER TABLE `name`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `offer`
--
ALTER TABLE `offer`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quotation`
--
ALTER TABLE `quotation`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `quotation_details`
--
ALTER TABLE `quotation_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `receipt`
--
ALTER TABLE `receipt`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `receipt_details`
--
ALTER TABLE `receipt_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
