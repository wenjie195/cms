-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 13, 2021 at 10:33 AM
-- Server version: 10.4.16-MariaDB
-- PHP Version: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vidatech_cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `bill_to` varchar(255) DEFAULT NULL,
  `term` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `uid`, `name`, `bill_to`, `term`, `amount`, `date`, `user_uid`, `status`, `date_created`, `date_updated`) VALUES
(1, 'c384a57c5844ba30a395545c5fe30aef', 'Twiggy (Pulau Tikus) Sdn Bhd1617088514', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '1699', '2020-10-23', NULL, 'Convert', '2021-03-30 07:18:40', '2021-03-30 07:36:08'),
(2, '95be2f94189c96ca69d59d5f1de84c3e', 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '1699', '2020-12-01', NULL, 'Convert', '2021-03-30 07:43:29', '2021-03-31 06:59:30'),
(3, '483f585305a869c02a90e04427565688', 'Twiggy (Pulau Tikus) Sdn Bhd1617090483', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '1699', '2021-01-04', NULL, 'Convert', '2021-03-30 07:48:40', '2021-03-31 06:59:33'),
(4, 'f886979eec6cb8608073e67c06ba2ad5', 'Twiggy (Pulau Tikus) Sdn Bhd1617090582', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '1699', '2021-02-15', NULL, 'Convert', '2021-03-30 07:50:18', '2021-03-31 06:59:35'),
(5, '1b2b010b8e1407df29bbc5aae05c4024', '56 Hair Saloon1617091356', '56 Hair Saloon', 'Campaign', '588', '2020-10-23', NULL, 'Convert', '2021-03-30 08:06:11', '2021-03-31 06:59:37'),
(6, '058e75651b6bbec90ce11951941d3250', '56 Hair Saloon1617091634', '56 Hair Saloon', 'Campaign', '588', '2020-12-01', NULL, 'Convert', '2021-03-30 08:07:49', '2021-03-31 06:59:39'),
(7, 'cdf7d892cd645f2867a91c9850e46156', '56 Hair Saloon1617091707', '56 Hair Saloon', 'Campaign', '588', '2021-01-04', NULL, 'Convert', '2021-03-30 08:08:52', '2021-03-31 06:59:41'),
(8, 'c5ed99efdcc203e82d1309e37647e41a', '56 Hair Saloon1617091772', '56 Hair Saloon', 'Campaign', '588', '2021-02-15', NULL, 'Convert', '2021-03-30 08:09:53', '2021-03-31 06:59:43'),
(9, '696f479afaa3dc31e6121f1f566e84cf', 'Twiggy (Pulau Tikus) Sdn Bhd1617358329', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Bank in', '2018.07', '2021-03-15', NULL, 'Pending', '2021-04-02 10:19:51', '2021-04-02 10:19:51'),
(10, '82b41ccd3cf08cd6380d25126e647520', '56 Hair Saloon1617358819', '56 Hair Saloon', 'Bank in', '325.49', '2021-03-15', NULL, 'Pending', '2021-04-02 10:25:35', '2021-04-02 10:25:35'),
(11, '1983b3552e0d053d943d5f75c87ddfd0', 'Aryss1617764857', 'Aryss', 'Bank in', '700', '2021-04-07', NULL, 'Pending', '2021-04-07 03:10:31', '2021-04-07 03:10:31');

-- --------------------------------------------------------

--
-- Table structure for table `invoice_details`
--

CREATE TABLE `invoice_details` (
  `id` bigint(20) NOT NULL,
  `quotation_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `unit_price` varchar(255) DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `total` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_details`
--

INSERT INTO `invoice_details` (`id`, `quotation_uid`, `uid`, `product_name`, `quantity`, `unit_price`, `uom`, `description`, `total`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Twiggy (Pulau Tikus) Sdn Bhd1617088514', '3866fd50c9db80b7708720ca9d84bb2e', 'Social Media Digital Marketing Packages', '1', '1699', '1', NULL, '1699', 'Delete', '2021-03-30 07:18:40', '2021-03-30 08:56:56'),
(2, 'Twiggy (Pulau Tikus) Sdn Bhd1617088514', '6fdf5061f92968f2586a19f6678c236c', 'Basic Marketing Package (Monthly)', '1', '1699', '', NULL, '1699', 'Convert', '2021-03-30 07:35:05', '2021-03-30 07:36:08'),
(3, 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', 'b94cc785632264da4804d53239af4616', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Convert', '2021-03-30 07:43:29', '2021-03-31 06:59:30'),
(4, 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', '814f6e545033f307ad307974247c9214', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Convert', '2021-03-30 07:43:29', '2021-03-31 06:59:30'),
(5, 'Twiggy (Pulau Tikus) Sdn Bhd1617090483', 'a0a53b0c6868e04024ccf97e4b674f6a', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Convert', '2021-03-30 07:48:40', '2021-03-31 06:59:33'),
(6, 'Twiggy (Pulau Tikus) Sdn Bhd1617090582', 'f4046634d0610b5db9f90d999107d574', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Convert', '2021-03-30 07:50:18', '2021-03-31 06:59:35'),
(7, '56 Hair Saloon1617091356', 'ac403396b32846f68d71579a53573b7e', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Convert', '2021-03-30 08:06:11', '2021-03-31 06:59:37'),
(8, '56 Hair Saloon1617091634', '975be16047152858de48dd0504dddd52', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Convert', '2021-03-30 08:07:49', '2021-03-31 06:59:39'),
(9, '56 Hair Saloon1617091707', 'f8939e53aef609a3e0a7442792453cc9', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Convert', '2021-03-30 08:08:52', '2021-03-31 06:59:41'),
(10, '56 Hair Saloon1617091772', '5c58282dd11155274b4a840268d8eccc', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Convert', '2021-03-30 08:09:53', '2021-03-31 06:59:43'),
(11, 'Twiggy (Pulau Tikus) Sdn Bhd1617358329', '806c5a8fd569af111c84f550994a16ac', 'Basic Marketing Packages (Monthly)', '1', '1699', 'MONTH', '- To provide marketing services on Facebook\r\n- 10 posting per month\r\n- Graphic/ads design for the posting content\r\n- Copywriting for the posting content\r\n- Marketing strategies planning', '1699', 'Pending', '2021-04-02 10:19:51', '2021-04-02 10:19:51'),
(12, 'Twiggy (Pulau Tikus) Sdn Bhd1617358329', 'df0d54cb4053bd4e896aec10dd79a783', 'Social Media Ads Boosting Fee', '1', '319.07', 'MONTH', '- To schedule social media ads boosting\r\n- Facebook SST charges (6%)\r\n[Calc: RM301.01 x 6%(GST)]', '319.07', 'Pending', '2021-04-02 10:19:51', '2021-04-02 10:19:51'),
(13, '56 Hair Saloon1617358819', '1e1d841fce4c492245737d89d8eee91b', 'Social Media Ads Boosting Fee', '1', '325.49', 'MONTH', '- To schedule social media ads boosting\r\n- Facebook SST charges (6%)\r\n[Calc: RM347.91 x 6%(GST)]', '325.49', 'Delete', '2021-04-02 10:25:35', '2021-04-02 10:28:55'),
(14, '56 Hair Saloon1617358819', '738779ec51eb4fe67dede1d9303a6ec2', 'Social Media Ads Boosting Fee', '1', '325.49', 'MONTH', '- To schedule social media ads boosting\r\n\r\n- Facebook SST charges (6%)\r\n[Calc: RMRM307.07 x 6%(GST)]\r\n', '325.49', 'Pending', '2021-04-02 10:25:35', '2021-04-02 10:25:35'),
(15, 'Aryss1617764857', '54ace4a568da860b559b32be3aeafe01', 'Page Creation', '1', '200', '', '- Complete all FB page settings\r\n- Set up Automated Response on Messenger\r\n- Activate an Ad Account for FB page advertising purposes \r\n', '200', 'Pending', '2021-04-07 03:10:31', '2021-04-07 03:10:31'),
(16, 'Aryss1617764857', '4c307e3840c872b2a17e02a66f99e3db', 'Logo Design', '1', '500', '', '- 2 drafts (As according to our suggestion / client preferences)\r\n- 3 edits allowed after designs are completed \r\n- JPG and PNG file will be provided to client', '500', 'Pending', '2021-04-07 03:10:31', '2021-04-07 03:10:31');

-- --------------------------------------------------------

--
-- Table structure for table `leave_status`
--

CREATE TABLE `leave_status` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `start_date` varchar(255) DEFAULT NULL,
  `end_date` varchar(255) DEFAULT NULL,
  `total_days` varchar(255) DEFAULT NULL,
  `reason` text DEFAULT NULL,
  `doc_one` varchar(255) DEFAULT NULL,
  `doc_two` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `approved_date` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `name`
--

CREATE TABLE `name` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `remark` text DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `offer`
--

CREATE TABLE `offer` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `ic_no` varchar(255) DEFAULT NULL,
  `address_one` text DEFAULT NULL,
  `address_two` text DEFAULT NULL,
  `address_three` text DEFAULT NULL,
  `address_four` text DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `position` text DEFAULT NULL,
  `start_date` varchar(255) DEFAULT NULL,
  `working_hrs` varchar(255) DEFAULT NULL,
  `salary` varchar(255) DEFAULT NULL,
  `probation` varchar(255) DEFAULT NULL,
  `job_scope` text DEFAULT NULL,
  `confirm_date` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pageview`
--

CREATE TABLE `pageview` (
  `id` int(255) NOT NULL,
  `page` text DEFAULT NULL,
  `ip_address` text DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `user_agent` text DEFAULT NULL,
  `device_type` text DEFAULT NULL,
  `browser_type` text DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `ic_no` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `join_date` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `epf_no` varchar(255) DEFAULT NULL,
  `account_no` varchar(255) DEFAULT NULL,
  `income_tax_no` varchar(255) DEFAULT NULL,
  `bank` varchar(255) DEFAULT NULL,
  `basic_pay` varchar(255) DEFAULT NULL,
  `allowance` varchar(255) DEFAULT NULL,
  `commission` varchar(255) DEFAULT NULL,
  `bonus` varchar(255) DEFAULT NULL,
  `overtime_hrs` varchar(255) DEFAULT NULL,
  `overtime_cost` varchar(255) DEFAULT NULL,
  `epf` varchar(255) DEFAULT NULL,
  `perkeso` varchar(255) DEFAULT NULL,
  `eis` varchar(255) DEFAULT NULL,
  `pcb` varchar(255) DEFAULT NULL,
  `unpaid_leave` varchar(255) DEFAULT NULL,
  `salary_after_ul` varchar(255) DEFAULT NULL,
  `epf_comp` varchar(255) DEFAULT NULL,
  `perkeso_comp` varchar(255) DEFAULT NULL,
  `eis_comp` varchar(255) DEFAULT NULL,
  `gross_income` varchar(255) DEFAULT NULL,
  `net_income` varchar(255) DEFAULT NULL,
  `gross_total` varchar(255) DEFAULT NULL,
  `net_month_pay` varchar(255) DEFAULT NULL,
  `al_entitle` varchar(255) DEFAULT NULL,
  `al_applied` varchar(255) DEFAULT NULL,
  `al_balance` varchar(255) DEFAULT NULL,
  `ml_entitle` varchar(255) DEFAULT NULL,
  `ml_applied` varchar(255) DEFAULT NULL,
  `ml_balanced` varchar(255) DEFAULT NULL,
  `month` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `date_created` timestamp(1) NOT NULL DEFAULT current_timestamp(1),
  `date_updated` timestamp(1) NOT NULL DEFAULT current_timestamp(1) ON UPDATE current_timestamp(1)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `printscreen`
--

CREATE TABLE `printscreen` (
  `id` int(255) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `page` text DEFAULT NULL,
  `ip_address` text DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `category` varchar(255) DEFAULT NULL,
  `product_code` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `part_number` varchar(255) DEFAULT NULL,
  `brand` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `cost` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `expired_date` varchar(255) DEFAULT NULL,
  `maintenance` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `quotation`
--

CREATE TABLE `quotation` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `bill_to` varchar(255) DEFAULT NULL,
  `term` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quotation`
--

INSERT INTO `quotation` (`id`, `uid`, `name`, `bill_to`, `term`, `amount`, `date`, `user_uid`, `status`, `date_created`, `date_updated`) VALUES
(1, 'c384a57c5844ba30a395545c5fe30aef', 'Twiggy (Pulau Tikus) Sdn Bhd1617088514', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '1699', '2020-10-23', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-03-30 07:17:22', '2021-03-30 07:18:40'),
(2, '95be2f94189c96ca69d59d5f1de84c3e', 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '1699', '2020-12-01', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-03-30 07:41:55', '2021-03-30 07:43:29'),
(3, '483f585305a869c02a90e04427565688', 'Twiggy (Pulau Tikus) Sdn Bhd1617090483', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '1699', '2021-01-04', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-03-30 07:48:15', '2021-03-30 07:48:40'),
(4, 'f886979eec6cb8608073e67c06ba2ad5', 'Twiggy (Pulau Tikus) Sdn Bhd1617090582', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '1699', '2021-02-15', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-03-30 07:49:49', '2021-03-30 07:50:18'),
(5, '1b2b010b8e1407df29bbc5aae05c4024', '56 Hair Saloon1617091356', '56 Hair Saloon', 'Campaign', '588', '2020-10-23', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-03-30 08:04:38', '2021-03-30 08:06:11'),
(6, '058e75651b6bbec90ce11951941d3250', '56 Hair Saloon1617091634', '56 Hair Saloon', 'Campaign', '588', '2020-12-01', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-03-30 08:07:38', '2021-03-30 08:07:49'),
(7, 'cdf7d892cd645f2867a91c9850e46156', '56 Hair Saloon1617091707', '56 Hair Saloon', 'Campaign', '588', '2021-01-04', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-03-30 08:08:37', '2021-03-30 08:08:52'),
(8, 'c5ed99efdcc203e82d1309e37647e41a', '56 Hair Saloon1617091772', '56 Hair Saloon', 'Campaign', '588', '2021-02-15', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-03-30 08:09:39', '2021-03-30 08:09:53'),
(9, '696f479afaa3dc31e6121f1f566e84cf', 'Twiggy (Pulau Tikus) Sdn Bhd1617358329', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Bank in', '2018.07', '2021-03-15', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-04-02 10:13:27', '2021-04-02 10:19:51'),
(10, '82b41ccd3cf08cd6380d25126e647520', '56 Hair Saloon1617358819', '56 Hair Saloon', 'Bank in', '325.49', '2021-03-15', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-04-02 10:21:17', '2021-04-02 10:25:35'),
(11, '065ffcd7bb396683577278d25986b476', 'Aryss1617764449', 'Aryss', 'Bank in', '700', '2021-04-07', '0b58991eff95670cb54b58a8c2f6ecf4', 'Pending', '2021-04-07 03:04:13', '2021-04-07 03:06:53'),
(12, '1983b3552e0d053d943d5f75c87ddfd0', 'Aryss1617764857', 'Aryss', 'Bank in', '700', '2021-04-07', '0b58991eff95670cb54b58a8c2f6ecf4', 'Convert', '2021-04-07 03:08:48', '2021-04-07 03:10:31');

-- --------------------------------------------------------

--
-- Table structure for table `quotation_details`
--

CREATE TABLE `quotation_details` (
  `id` bigint(20) NOT NULL,
  `quotation_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `unit_price` varchar(255) DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `total` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quotation_details`
--

INSERT INTO `quotation_details` (`id`, `quotation_uid`, `uid`, `product_name`, `quantity`, `unit_price`, `uom`, `description`, `total`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Twiggy (Pulau Tikus) Sdn Bhd1617088514', '3866fd50c9db80b7708720ca9d84bb2e', 'Social Media Digital Marketing Packages', '1', '1699', '1', NULL, '1699', 'Convert', '2021-03-30 07:17:22', '2021-03-30 07:18:40'),
(2, 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', 'b94cc785632264da4804d53239af4616', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Convert', '2021-03-30 07:41:55', '2021-03-30 07:43:29'),
(3, 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', '814f6e545033f307ad307974247c9214', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Convert', '2021-03-30 07:43:04', '2021-03-30 07:43:29'),
(4, 'Twiggy (Pulau Tikus) Sdn Bhd1617090483', 'a0a53b0c6868e04024ccf97e4b674f6a', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Convert', '2021-03-30 07:48:15', '2021-03-30 07:48:40'),
(5, 'Twiggy (Pulau Tikus) Sdn Bhd1617090582', 'f4046634d0610b5db9f90d999107d574', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Convert', '2021-03-30 07:49:49', '2021-03-30 07:50:18'),
(6, '56 Hair Saloon1617091356', 'ac403396b32846f68d71579a53573b7e', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Convert', '2021-03-30 08:04:38', '2021-03-30 08:06:11'),
(7, '56 Hair Saloon1617091634', '975be16047152858de48dd0504dddd52', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Convert', '2021-03-30 08:07:38', '2021-03-30 08:07:49'),
(8, '56 Hair Saloon1617091707', 'f8939e53aef609a3e0a7442792453cc9', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Convert', '2021-03-30 08:08:37', '2021-03-30 08:08:52'),
(9, '56 Hair Saloon1617091772', '5c58282dd11155274b4a840268d8eccc', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Convert', '2021-03-30 08:09:39', '2021-03-30 08:09:53'),
(10, 'Twiggy (Pulau Tikus) Sdn Bhd1617358329', '806c5a8fd569af111c84f550994a16ac', 'Basic Marketing Packages (Monthly)', '1', '1699', 'MONTH', '- To provide marketing services on Facebook\r\n- 10 posting per month\r\n- Graphic/ads design for the posting content\r\n- Copywriting for the posting content\r\n- Marketing strategies planning', '1699', 'Convert', '2021-04-02 10:13:27', '2021-04-02 10:19:51'),
(11, 'Twiggy (Pulau Tikus) Sdn Bhd1617358329', 'df0d54cb4053bd4e896aec10dd79a783', 'Social Media Ads Boosting Fee', '1', '319.07', 'MONTH', '- To schedule social media ads boosting\r\n- Facebook SST charges (6%)\r\n[Calc: RM301.01 x 6%(GST)]', '319.07', 'Convert', '2021-04-02 10:16:36', '2021-04-02 10:19:51'),
(12, '56 Hair Saloon1617358819', '1e1d841fce4c492245737d89d8eee91b', 'Social Media Ads Boosting Fee', '1', '325.49', 'MONTH', '- To schedule social media ads boosting\r\n- Facebook SST charges (6%)\r\n[Calc: RM347.91 x 6%(GST)]', '325.49', 'Convert', '2021-04-02 10:21:17', '2021-04-02 10:25:35'),
(13, '56 Hair Saloon1617358819', '738779ec51eb4fe67dede1d9303a6ec2', 'Social Media Ads Boosting Fee', '1', '325.49', 'MONTH', '- To schedule social media ads boosting\r\n\r\n- Facebook SST charges (6%)\r\n[Calc: RMRM307.07 x 6%(GST)]\r\n', '325.49', 'Convert', '2021-04-02 10:24:16', '2021-04-02 10:25:35'),
(14, 'Aryss1617764449', '002f72d305b6210d64f893e8cdf6cd43', 'Page Creation', '1', '200', '', 'Page Creation\r\n- Complete all FB page settings\r\n- Set up Automated Response on Messenger\r\n- Activate an Ad Account for FB page advertising purposes ', '200', 'Pending', '2021-04-07 03:04:13', '2021-04-07 03:04:13'),
(15, 'Aryss1617764449', 'c70991732afca4941846ac7987d1a225', 'Logo Design', '1', '500', '', 'Logo Design\r\n- 2 drafts (As according to our suggestion / client preferences)\r\n- 3 edits allowed after designs are completed \r\n- JPG and PNG file will be provided to client', '500', 'Pending', '2021-04-07 03:06:48', '2021-04-07 03:06:48'),
(16, 'Aryss1617764857', '54ace4a568da860b559b32be3aeafe01', 'Page Creation', '1', '200', '', '- Complete all FB page settings\r\n- Set up Automated Response on Messenger\r\n- Activate an Ad Account for FB page advertising purposes \r\n', '200', 'Convert', '2021-04-07 03:08:48', '2021-04-07 03:10:31'),
(17, 'Aryss1617764857', '4c307e3840c872b2a17e02a66f99e3db', 'Logo Design', '1', '500', '', '- 2 drafts (As according to our suggestion / client preferences)\r\n- 3 edits allowed after designs are completed \r\n- JPG and PNG file will be provided to client', '500', 'Convert', '2021-04-07 03:09:17', '2021-04-07 03:10:31');

-- --------------------------------------------------------

--
-- Table structure for table `receipt`
--

CREATE TABLE `receipt` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `bill_to` varchar(255) DEFAULT NULL,
  `term` varchar(255) DEFAULT NULL,
  `amount` varchar(255) DEFAULT NULL,
  `date` varchar(255) DEFAULT NULL,
  `month` varchar(255) DEFAULT NULL,
  `year` varchar(255) DEFAULT NULL,
  `user_uid` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `receipt`
--

INSERT INTO `receipt` (`id`, `uid`, `name`, `bill_to`, `term`, `amount`, `date`, `month`, `year`, `user_uid`, `status`, `date_created`, `date_updated`) VALUES
(1, 'c384a57c5844ba30a395545c5fe30aef', 'Twiggy (Pulau Tikus) Sdn Bhd1617088514', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '1699', '2020-10-23', 'Nov', '2020', NULL, 'Closed', '2021-03-30 07:36:08', '2021-03-30 07:36:08'),
(2, '95be2f94189c96ca69d59d5f1de84c3e', 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '2179.9', '2020-12-01', 'Dec', '2020', NULL, 'Closed', '2021-03-31 06:59:30', '2021-03-31 06:59:30'),
(3, '483f585305a869c02a90e04427565688', 'Twiggy (Pulau Tikus) Sdn Bhd1617090483', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '2353.21', '2021-01-04', 'Jan', '2020', NULL, 'Closed', '2021-03-31 06:59:33', '2021-03-31 06:59:33'),
(4, 'f886979eec6cb8608073e67c06ba2ad5', 'Twiggy (Pulau Tikus) Sdn Bhd1617090582', 'Twiggy (Pulau Tikus) Sdn Bhd', 'Monthly', '2067.78', '2021-02-15', 'Feb', '2021', NULL, 'Closed', '2021-03-31 06:59:35', '2021-03-31 06:59:35'),
(5, '1b2b010b8e1407df29bbc5aae05c4024', '56 Hair Saloon1617091356', '56 Hair Saloon', 'Campaign', '588', '2020-10-23', 'Oct', '2020', NULL, 'Closed', '2021-03-31 06:59:37', '2021-03-31 06:59:37'),
(6, '058e75651b6bbec90ce11951941d3250', '56 Hair Saloon1617091634', '56 Hair Saloon', 'Campaign', '909.62', '2020-12-01', 'Dec', '2020', NULL, 'Closed', '2021-03-31 06:59:39', '2021-03-31 06:59:39'),
(7, 'cdf7d892cd645f2867a91c9850e46156', '56 Hair Saloon1617091707', '56 Hair Saloon', 'Campaign', '1656.8', '2021-01-04', 'Jan', '2021', NULL, 'Closed', '2021-03-31 06:59:41', '2021-03-31 06:59:41'),
(8, 'c5ed99efdcc203e82d1309e37647e41a', '56 Hair Saloon1617091772', '56 Hair Saloon', 'Campaign', '1027.61', '2021-02-15', 'Feb', '2021', NULL, 'Closed', '2021-03-31 06:59:43', '2021-03-31 06:59:43');

-- --------------------------------------------------------

--
-- Table structure for table `receipt_details`
--

CREATE TABLE `receipt_details` (
  `id` bigint(20) NOT NULL,
  `quotation_uid` varchar(255) DEFAULT NULL,
  `uid` varchar(255) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) DEFAULT NULL,
  `unit_price` varchar(255) DEFAULT NULL,
  `uom` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `total` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `receipt_details`
--

INSERT INTO `receipt_details` (`id`, `quotation_uid`, `uid`, `product_name`, `quantity`, `unit_price`, `uom`, `description`, `total`, `status`, `date_created`, `date_updated`) VALUES
(1, 'Twiggy (Pulau Tikus) Sdn Bhd1617088514', '3866fd50c9db80b7708720ca9d84bb2e', 'Social Media Digital Marketing Packages', '1', '1699', '1', NULL, '1699', 'Closed', '2021-03-30 07:36:08', '2021-03-30 07:36:08'),
(2, 'Twiggy (Pulau Tikus) Sdn Bhd1617088514', '6fdf5061f92968f2586a19f6678c236c', 'Basic Marketing Package (Monthly)', '1', '1699', '', NULL, '1699', 'Closed', '2021-03-30 07:36:08', '2021-03-30 07:36:08'),
(3, 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', 'b94cc785632264da4804d53239af4616', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Closed', '2021-03-31 06:59:30', '2021-03-31 06:59:30'),
(4, 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', '814f6e545033f307ad307974247c9214', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Closed', '2021-03-31 06:59:30', '2021-03-31 06:59:30'),
(5, 'Twiggy (Pulau Tikus) Sdn Bhd1617090483', 'a0a53b0c6868e04024ccf97e4b674f6a', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Closed', '2021-03-31 06:59:33', '2021-03-31 06:59:33'),
(6, 'Twiggy (Pulau Tikus) Sdn Bhd1617090582', 'f4046634d0610b5db9f90d999107d574', 'Basic Marketing Packages (Monthly)', '1', '1699', '1', NULL, '1699', 'Closed', '2021-03-31 06:59:35', '2021-03-31 06:59:35'),
(7, '56 Hair Saloon1617091356', 'ac403396b32846f68d71579a53573b7e', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Closed', '2021-03-31 06:59:37', '2021-03-31 06:59:37'),
(8, '56 Hair Saloon1617091634', '975be16047152858de48dd0504dddd52', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Closed', '2021-03-31 06:59:39', '2021-03-31 06:59:39'),
(9, '56 Hair Saloon1617091707', 'f8939e53aef609a3e0a7442792453cc9', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Closed', '2021-03-31 06:59:41', '2021-03-31 06:59:41'),
(10, '56 Hair Saloon1617091772', '5c58282dd11155274b4a840268d8eccc', 'Marketing Campaign Packages (Campaign-based)', '1', '588', '1', NULL, '588', 'Closed', '2021-03-31 06:59:43', '2021-03-31 06:59:43'),
(11, 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', '411f4b82b241b6f19b370aa95055710d', 'Social Media Ads Boosting Fee (November 2020)', '1', '480.9', '', NULL, '480.9', 'Closed', '2021-03-31 07:02:37', '2021-03-31 07:02:37'),
(12, 'Twiggy (Pulau Tikus) Sdn Bhd1617090071', '28832eb9c4d70b36e5c45d167ac83e25', 'Social Media Ads Boosting Fee (November 2020)', '1', '480.9', '1', NULL, '480.9', 'Closed', '2021-03-31 07:03:02', '2021-03-31 07:03:02'),
(13, '56 Hair Saloon1617091634', '34edf43002f6bfaf260c2439b97b1d09', 'Social Media Ads Boosting Fee (November 2020)', '1', '321.62', '1', NULL, '321.62', 'Closed', '2021-03-31 07:03:47', '2021-03-31 07:03:47'),
(14, 'Twiggy (Pulau Tikus) Sdn Bhd1617090483', 'e5942997b1a8dcf0649f78cde660d3f3', 'Social Media Ads Boosting Fee (December 2020)', '1', '654.21', '1', NULL, '654.21', 'Closed', '2021-03-31 07:04:51', '2021-03-31 07:04:51'),
(15, '56 Hair Saloon1617091707', '611db3c1b1d1c98c21debb2ab277cfd6', 'Social Media Ads Boosting Fee (December 2020)', '1', '1068.8', '1', NULL, '1068.8', 'Closed', '2021-03-31 07:05:43', '2021-03-31 07:05:43'),
(16, 'Twiggy (Pulau Tikus) Sdn Bhd1617090582', 'c6144d7d9cfa8d7d35aa5fea59ac7e09', 'Social Media Ads Boosting Fee (January 2021)', '1', '368.78', '1', NULL, '368.78', 'Closed', '2021-03-31 07:07:02', '2021-03-31 07:07:02'),
(17, '56 Hair Saloon1617091772', '5023a6f127fcfd5cba5f19c1cf802337', 'Social Media Ads Boosting Fee (January 2021)', '1', '439.61', '1', NULL, '439.61', 'Closed', '2021-03-31 07:08:06', '2021-03-31 07:08:06');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(255) DEFAULT NULL,
  `sales` varchar(255) DEFAULT NULL,
  `salary` varchar(255) DEFAULT NULL,
  `epf` varchar(255) DEFAULT NULL,
  `socso` varchar(255) DEFAULT NULL,
  `eis` varchar(255) DEFAULT NULL,
  `pcb` varchar(255) DEFAULT NULL,
  `rental` varchar(255) DEFAULT NULL,
  `expenses` varchar(255) DEFAULT NULL,
  `profit` varchar(255) DEFAULT NULL,
  `month` varchar(255) DEFAULT NULL,
  `years` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL,
  `uid` varchar(200) DEFAULT NULL COMMENT 'random user id',
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` char(64) NOT NULL,
  `salt` char(64) NOT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `ic_no` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `emergency_contact` varchar(255) DEFAULT NULL,
  `emergency_ppl` varchar(255) DEFAULT NULL,
  `salary` varchar(255) DEFAULT NULL,
  `allowance` varchar(255) DEFAULT NULL,
  `leave_total` varchar(255) DEFAULT NULL,
  `leave_applied` varchar(255) DEFAULT NULL,
  `join_date` varchar(255) DEFAULT NULL,
  `epf` varchar(255) DEFAULT NULL,
  `epf_no` varchar(255) DEFAULT NULL,
  `sosco` varchar(255) DEFAULT NULL,
  `sosco_no` varchar(255) DEFAULT NULL,
  `lhdn` varchar(255) DEFAULT NULL,
  `lhdn_no` varchar(255) DEFAULT NULL,
  `login_type` int(2) NOT NULL DEFAULT 1 COMMENT '1 = normal',
  `user_type` int(2) NOT NULL DEFAULT 1 COMMENT '0 = admin, 1 = normal user',
  `date_created` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_updated` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `uid`, `username`, `email`, `password`, `salt`, `fullname`, `ic_no`, `phone`, `address`, `emergency_contact`, `emergency_ppl`, `salary`, `allowance`, `leave_total`, `leave_applied`, `join_date`, `epf`, `epf_no`, `sosco`, `sosco_no`, `lhdn`, `lhdn_no`, `login_type`, `user_type`, `date_created`, `date_updated`) VALUES
(1, '9ca817799c575c1afd37cccf6a22aa7f', 'admin', 'admin@gmail.com', 'fe8192029d7b39d66fe035ea96623f6ab6f06c961985d12f210b353764b1e90a', 'e74df95cbd449eb12c45c8685694897a6869037f', 'admin', '12312301', '01212312301', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, '2020-01-14 06:36:33', '2021-03-19 05:32:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice_details`
--
ALTER TABLE `invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leave_status`
--
ALTER TABLE `leave_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `name`
--
ALTER TABLE `name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offer`
--
ALTER TABLE `offer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pageview`
--
ALTER TABLE `pageview`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `printscreen`
--
ALTER TABLE `printscreen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation`
--
ALTER TABLE `quotation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation_details`
--
ALTER TABLE `quotation_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt`
--
ALTER TABLE `receipt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_details`
--
ALTER TABLE `receipt_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `invoice_details`
--
ALTER TABLE `invoice_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `leave_status`
--
ALTER TABLE `leave_status`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `name`
--
ALTER TABLE `name`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `offer`
--
ALTER TABLE `offer`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pageview`
--
ALTER TABLE `pageview`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `printscreen`
--
ALTER TABLE `printscreen`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quotation`
--
ALTER TABLE `quotation`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `quotation_details`
--
ALTER TABLE `quotation_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `receipt`
--
ALTER TABLE `receipt`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `receipt_details`
--
ALTER TABLE `receipt_details`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
