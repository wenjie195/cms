<?php  
class Offer {
    /* Member variables */
    var $id,$uid,$name,$icno,$addressOne,$addressTwo,$addressThree,$addressFour,$date,$position,$startDate,$workingHrs,$salary,$probation,$jobScope,$confirmDate,$status,
            $dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getIcno()
    {
        return $this->icno;
    }

    /**
     * @param mixed $icno
     */
    public function setIcno($icno)
    {
        $this->icno = $icno;
    }

    /**
     * @return mixed
     */
    public function getAddressOne()
    {
        return $this->addressOne;
    }

    /**
     * @param mixed $addressOne
     */
    public function setAddressOne($addressOne)
    {
        $this->addressOne = $addressOne;
    }

    /**
     * @return mixed
     */
    public function getAddressTwo()
    {
        return $this->addressTwo;
    }

    /**
     * @param mixed $addressTwo
     */
    public function setAddressTwo($addressTwo)
    {
        $this->addressTwo = $addressTwo;
    }

    /**
     * @return mixed
     */
    public function getAddressThree()
    {
        return $this->addressThree;
    }

    /**
     * @param mixed $addressThree
     */
    public function setAddressThree($addressThree)
    {
        $this->addressThree = $addressThree;
    }

    /**
     * @return mixed
     */
    public function getAddressFour()
    {
        return $this->addressFour;
    }

    /**
     * @param mixed $addressFour
     */
    public function setAddressFour($addressFour)
    {
        $this->addressFour = $addressFour;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param mixed $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return mixed
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * @param mixed $startDate
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }

    /**
     * @return mixed
     */
    public function getWorkingHrs()
    {
        return $this->workingHrs;
    }

    /**
     * @param mixed $workingHrs
     */
    public function setWorkingHrs($workingHrs)
    {
        $this->workingHrs = $workingHrs;
    }

    /**
     * @return mixed
     */
    public function getSalary()
    {
        return $this->salary;
    }

    /**
     * @param mixed $salary
     */
    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

    /**
     * @return mixed
     */
    public function getProbation()
    {
        return $this->probation;
    }

    /**
     * @param mixed $probation
     */
    public function setProbation($probation)
    {
        $this->probation = $probation;
    }

    /**
     * @return mixed
     */
    public function getJobScope()
    {
        return $this->jobScope;
    }

    /**
     * @param mixed $jobScope
     */
    public function setJobScope($jobScope)
    {
        $this->jobScope = $jobScope;
    }

    /**
     * @return mixed
     */
    public function getConfirmDate()
    {
        return $this->confirmDate;
    }

    /**
     * @param mixed $confirmDate
     */
    public function setConfirmDate($confirmDate)
    {
        $this->confirmDate = $confirmDate;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getOffer($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","name","ic_no","address_one","address_two","address_three","address_four","date","position","start_date","working_hrs","salary",
                            "probation","job_scope","confirm_date","status","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"offer");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$name,$icno,$addressOne,$addressTwo,$addressThree,$addressFour,$date,$position,$startDate,$workingHrs,$salary,$probation,$jobScope,
                                $confirmDate,$status,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Offer;
            $class->setId($id);
            $class->setUid($uid);
            $class->setName($name);

            $class->setIcno($icno);
            $class->setAddressOne($addressOne);
            $class->setAddressTwo($addressTwo);
            $class->setAddressThree($addressThree);
            $class->setAddressFour($addressFour);
            $class->setDate($date);
            $class->setPosition($position);
            $class->setStartDate($startDate);
            $class->setWorkingHrs($workingHrs);
            $class->setSalary($salary);
            $class->setProbation($probation);
            $class->setJobScope($jobScope);
            $class->setConfirmDate($confirmDate);

            $class->setStatus($status);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
          
            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
