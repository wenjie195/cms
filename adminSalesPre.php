<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Sales Details | CMS" />
    <title>Sales Details | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
    <?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>
<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">

  <h1 class="h1-title open">Sales Details</h1>

  <div class="big-four-input-container">
    <form method='post' action='adminSalesAdd.php'> 
      <div class="input50-div">
        <p class="input-top-p">Month</p>
        <select class="clean tele-input" type="text" id="month" name="month" required>
          <option value="" name=" ">Please Select a Month</option>
          <option value="Jan" name="Jan">Jan</option>
          <option value="Feb" name="Feb">Feb</option>
          <option value="Mar" name="Mar">Mar</option>
          <option value="Apr" name="Apr">Apr</option>
          <option value="May" name="May">May</option>
          <option value="Jun" name="Jun">Jun</option>
          <option value="Jul" name="Jul">Jul</option>
          <option value="Aug" name="Aug">Aug</option>
          <option value="Sep" name="Sep">Sep</option>
          <option value="Oct" name="Oct">Oct</option>
          <option value="Nov" name="Nov">Nov</option>
          <option value="Dec" name="Dec">Dec</option>

          <!-- <option value="01" name="01">Jan</option>
          <option value="02" name="02">Feb</option>
          <option value="03" name="03">Mar</option>
          <option value="04" name="04">Apr</option>
          <option value="05" name="05">May</option>
          <option value="06" name="06">Jun</option>
          <option value="07" name="07">Jul</option>
          <option value="08" name="08">Aug</option>
          <option value="09" name="09">Sep</option>
          <option value="10" name="10">Oct</option>
          <option value="11" name="11">Nov</option>
          <option value="12" name="12">Dec</option> -->
        </select>
      </div>

      <div class="input50-div second-input50">
        <p class="input-top-p">Year</p>
        <input type="number" placeholder="Key in a year" class="clean tele-input" name='years' id="years" required>
      </div>

        <div class="clear"></div>

        <button class="clean blue-btn red-btn margin-top30 fix300-btn margin-bottom30" name="submit">Next</button>

        <div class="clear"></div>
      

    </form>         
  </div>

</div>

<style>
.sales-li{
	color:#264a9c;
	background-color:white;}
.sales-li .hover1a{
	display:none;}
.sales-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

</body>
</html>