<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Name.php';
// require_once dirname(__FILE__) . '/classes/Category.php';
require_once dirname(__FILE__) . '/classes/User.php';

// require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $allCategory = getCategory($conn);
$allName = getName($conn);

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'meta.php'; ?>
    <!-- <meta property="og:url" content="https://qlianmeng.asia/addReferee.php" /> -->
    <meta property="og:title" content="Add Supplier Name | CMS" />
    <title>Add Supplier Name | CMS</title>
    <!-- <link rel="canonical" href="https://qlianmeng.asia/addReferee.php" /> -->
	<?php include 'css.php'; ?>
</head>
<body class="body">

<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'adminSidebar.php'; ?>

<div class="next-to-sidebar">
	<h1 class="h1-title">Add Supplier Name</h1> 
    <!-- <form action="utilities/adminAddSupplierName.php" method="POST"> -->
    <form action="utilities/adminAddSupplierNameFunction.php" method="POST">

        <!-- <div class="width100"> -->
        <div class="input50-div">
            <p class="input-title-p">Supplier Name</p>
            <input class="clean tele-input" type="text" placeholder="Supplier Name" id="supplier_name" name="supplier_name" required>        
        </div> 

        <div class="input50-div second-input50">
            <p class="input-title-p">Phone</p>
            <input class="clean tele-input" type="text" placeholder="Phone" id="phone" name="phone" required>       
        </div> 

        <div class="clear"></div>

        <!-- <div class="width100">
            <p class="input-title-p">Address</p>    
            <textarea type="text" class="clean tele-input" rows="10" cols="80" placeholder="Address" id="address" name="address"></textarea>  
        </div>  -->

        <div class="width100">
            <p class="input-title-p">Address</p>
            <textarea  type="text" class="clean tele-input textarea-min-height" placeholder="Address" id="address" name="address"></textarea> 
        </div> 

        <div class="clear"></div>

        <div class="width100">
            <p class="input-title-p">Remark</p>    
            <textarea type="text" class="clean tele-input" rows="10" cols="80" placeholder="Remark" id="remark" name="remark"></textarea>  
        </div> 

        <div class="clear"></div>

        <button class="clean red-btn fix300-btn align-left" name="submit">Submit</button>

    </form>

    <div class="clear"></div>


    <div class="overflow-scroll-div margin-top20">
        <table class="shipping-table" id="myTable">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Name</th>

                    <th>Phone</th>
                    <th>Address</th>
                    <th>Remark</th>
                    <th>Edit</th>
                    <!-- <th>Delete</th> -->
                </tr>
            </thead>
            <tbody>
                <?php
                $conn = connDB();
                if($allName)
                {   
                    for($cnt = 0;$cnt < count($allName) ;$cnt++)
                    {
                    ?>
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $allName[$cnt]->getName();?></td>

                            <td><?php echo $allName[$cnt]->getPhone();?></td>
                            <td><?php echo $allName[$cnt]->getAddress();?></td>
                            <td><?php echo $allName[$cnt]->getRemark();?></td>

                            <td>
                                <form action="adminAddSupplierNameEdit.php" method="POST">
                                    <button class="clean hover1 img-btn" type="submit" name="user_uid" value="<?php echo $allName[$cnt]->getUid();?>">
                                        <img src="img/edit2.png" class="width100 hover1a" >
                                        <img src="img/edit3.png" class="width100 hover1b" >
                                    </button>
                                </form>
                            </td>

                            <!-- <td>
                                Delete
                            </td> -->
                        </tr>
                    <?php
                    }
                    ?>
                <?php
                }
                $conn->close();
                ?>
            </tbody>
        </table>
    </div>


</div>

<style>
.telemarketer-li{
	color:#264a9c;
	background-color:white;}
.telemarketer-li .hover1a{
	display:none;}
.telemarketer-li .hover1b{
	display:block;}
</style>

<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Supplier Name Updated !"; 
        }
        elseif($_GET['type'] == 2)
        {
            $messageType = "Fail To Edit Supplier Details !"; 
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>